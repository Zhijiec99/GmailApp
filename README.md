# Phase 4

This is the documentation for phase 4. Phase 4 is stored in master branch

## Assumptions
- You have a valid SQL connection from Net Beans.
- You have the Folder "C:/Temp/" created (with capital 't').
- You are on the latest version of master.

## Steps to Setup

1. Open the Project Cao in netbeans
2. Go in src/test/resources/ (In NetBeans: Other Test Sources -> src/test/resources/ - > database) and run:
	- CreateTestDB.sql
3. Go in src/test/resources/ (In NetBeans: Other Sources -> src/main/resources/ - > database) and run:
	- Create DB other.sql
	- Create DB receive.sql
	- Create DB send.sql
4. Right Click Cao, Run maven -> Goals -> Press ok.
	

## Using the Application
There are 3 main parts of this application
## Config Composition
You will start with a form, already filled up. Anything you want to modify will be here.
There are only 3 possible valid configurations:
### Send: 
```
userEmail=send.1633068@gmail.com
SmtpServerUrl=smtp.gmail.com
SmtpServerPort=465
mysqlDatabaseURL=localhost
mysqlDatabasePort=3306
ImapServerPort=993
databaseName=send
userName=send1633068
databaseUserName=send
databasePassword=zhijie123
userPassword=zhijie123
ImapServerUrl=imap.gmail.com

```
### Receive: 
```
userEmail=receive.1633068@gmail.com
SmtpServerUrl=smtp.gmail.com
SmtpServerPort=465
mysqlDatabaseURL=localhost
mysqlDatabasePort=3306
ImapServerPort=993
databaseName=receive
userName=receive1633068
databaseUserName=receive
databasePassword=zhijie123
userPassword=zhijie123
ImapServerUrl=imap.gmail.com

```
### Other: 
```
userEmail=other.1633068@gmail.com
SmtpServerUrl=smtp.gmail.com
SmtpServerPort=465
mysqlDatabaseURL=localhost
mysqlDatabasePort=3306
ImapServerPort=993
databaseName=other
userName=other1633068
databaseUserName=other
databasePassword=zhijie123
userPassword=zhijie123
ImapServerUrl=imap.gmail.com
```
## Mail Interface
This is where most of the interface is. 
You can: 
- add/delete folders.
- Create new mail.
- Reply, move, forward or delete a mail.
- And most importantly, view emails.
## Mail Composition
Here is where you compose a mail.
Adding an attachment will open a dialog and add an attachment.
Inserting will also add an img tag in the HTML code.

**IMPORTANT**
You **must** put multiple emails separated with spaces.
You **cannot** put the same email multiple times.
