/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Zhijie Cao
 * Created: 6-Nov-2018
 */

DROP DATABASE IF EXISTS TEST;
CREATE DATABASE TEST;

USE TEST;

DROP USER IF EXISTS test@localhost;
CREATE USER test@'localhost' IDENTIFIED WITH mysql_native_password BY 'zhijie123' REQUIRE NONE;
GRANT ALL ON test.* TO test@'localhost';


FLUSH PRIVILEGES;