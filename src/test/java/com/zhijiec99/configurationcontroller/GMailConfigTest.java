/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.configurationcontroller;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.*;

/**
 *
 * @author Zhijie Cao
 */
public class GMailConfigTest {
    
    @Before 
    public void init(){
        
    }
    
    @Test
    public void constructorTest() throws IOException{
        GMailConfig gmconf = new GMailConfig("./src/test/resources/receive_conf");
        Assert.assertNotNull(gmconf);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void constructorTestInvalidConfFile() throws IOException{
        GMailConfig gmconf = new GMailConfig("./src/test/resources/invalid_conf");
    }
    
    @Test(expected=FileNotFoundException.class)
    public void constructorTestUnexistantFile() throws IOException{
        GMailConfig gmconf = new GMailConfig("taedasdadddadw");
    }
    
}
