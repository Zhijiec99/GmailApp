/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.databasecontroller;

import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.configurationcontroller.GMailConfig;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Zhijie Cao
 */
public class FolderStorageModuleTest {

    private static FXPropertiesBean config;

    public FolderStorageModuleTest() throws IOException {
        GMailConfig sendGmailConf = new GMailConfig("src/test/resources/send_conf");
        config = new FXPropertiesBean(sendGmailConf.getUserName(),
                sendGmailConf.getUserEmail(),
                sendGmailConf.getUserPassword(),
                sendGmailConf.getImapServerUrl(),
                sendGmailConf.getSmtpServerUrl(),
                sendGmailConf.getImapServerPort(),
                sendGmailConf.getSmtpServerPort(),
                sendGmailConf.getMysqlDatabaseURL(),
                sendGmailConf.getMysqlDatabasePort(),
                sendGmailConf.getDatabaseName(),
                sendGmailConf.getDatabaseUserName(),
                sendGmailConf.getDatabasePassword());
    }

    @Test
    public void TestDeleteFolder() {
        try {
            FolderStorageModule fsm = new FolderStorageModule(config);
            fsm.deleteFolder("DELETED");
            Map<Integer, String> map = fsm.retrieveFolder();
            Assert.assertFalse(map.containsValue("DELETED"));
        } catch (SQLException ex) {
            Logger.getLogger(FolderStorageModuleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void TestCreateFolder() {

        try {
            FolderStorageModule fsm = new FolderStorageModule(config);
            fsm.createFolder("Hello");
            Map<Integer, String> map = fsm.retrieveFolder();
            Assert.assertTrue(map.containsValue("Hello"));
        } catch (SQLException ex) {
            Logger.getLogger(FolderStorageModuleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void TestCreateFolderEmpty() {
        try {
            FolderStorageModule fsm = new FolderStorageModule(config);
            fsm.createFolder("");
            Map<Integer, String> map = fsm.retrieveFolder();
            Assert.assertTrue(map.containsValue(""));
        } catch (SQLException ex) {
            Logger.getLogger(FolderStorageModuleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void TestCreateThenDeleteFolderEmpty() {

        try {
            FolderStorageModule fsm = new FolderStorageModule(config);
            fsm.createFolder("");
            fsm.deleteFolder("");
            Map<Integer, String> map = fsm.retrieveFolder();
            Assert.assertFalse(map.containsValue(""));
        } catch (SQLException ex) {
            Logger.getLogger(FolderStorageModuleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Before
    public void seedDatabase() {
        final String sqlFile = loadString("database/JAG_Create.sql");
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/test?zeroDateTimeBehavior=CONVERT_TO_NULL",
                config.getDatabaseUserName(),
                config.getDatabasePassword());) {
            connection.prepareStatement("USE " + config.getDatabaseName()).execute();
            for (String statement : splitStatements(new StringReader(sqlFile), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    private String loadString(String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path)) {
            Scanner scanner = new Scanner(inputStream);
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input Stream");
        }
    }

    private List<String> splitStatements(Reader reader, String stmtDelimiter) {
        BufferedReader buffReader = new BufferedReader(reader);
        StringBuilder sqlStmnt = new StringBuilder();
        List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = buffReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStmnt.append(line);
                if (line.endsWith(stmtDelimiter)) {
                    statements.add(sqlStmnt.toString());
                    sqlStmnt.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
