/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.databasecontroller;

import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.beans.AttachmentBean;
import com.zhijiec99.beans.MailBean;
import com.zhijiec99.configurationcontroller.GMailConfig;
import com.zhijiec99.data.EmailPriority;
import com.zhijiec99.data.MessageType;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import jodd.mail.EmailAddress;
import org.junit.*;
import org.junit.BeforeClass;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

/**
 * This is the test class for most of the MailStorageModule stuff.
 *
 * @author Zhijie Cao
 */
@RunWith(Parameterized.class)
public class MailStorageModuleTest {
    MailStorageModule ms;
    public MailStorageModuleTest() throws SQLException, IOException{
        GMailConfig sendGmailConf = new GMailConfig("src/test/resources/send_conf");
        config = new FXPropertiesBean(
                sendGmailConf.getUserName(),
                sendGmailConf.getUserEmail(),
                sendGmailConf.getUserPassword(),
                sendGmailConf.getImapServerUrl(),
                sendGmailConf.getSmtpServerUrl(),
                sendGmailConf.getImapServerPort(),
                sendGmailConf.getSmtpServerPort(),
                sendGmailConf.getMysqlDatabaseURL(),
                sendGmailConf.getMysqlDatabasePort(),
                sendGmailConf.getDatabaseName(),
                sendGmailConf.getDatabaseUserName(),
                sendGmailConf.getDatabasePassword());
            ms = new MailStorageModule(config);
    }
    private static FXPropertiesBean config;

    @Parameter(0)
    public MailBean bean;

    @Parameter(1)
    public Class<? extends Exception> expectedException;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    /**
     * This method test getAndSet.
     */
    public void retrieveMailTest() throws IOException {

        try {
        
            if (expectedException != null) {
                thrown.expect(expectedException);
            }

            int id = ms.insertMail(bean);
            MailBean queryBean = new MailBean(null, null, null, null, null, null, null, null, null, null, null, null, null, null //set the mailBean with one of the ids from the list
                    ,
                     id);

            List<MailBean> list = ms.retrieveMail(queryBean);

            Assert.assertEquals(id, list.get(0).getEmailId());
        } catch (SQLException ex) {
            Logger.getLogger(MailStorageModuleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void retrieveMailByFolderTest() {

        try {
            MailStorageModule ms = new MailStorageModule(config);
            if (expectedException != null) {
                thrown.expect(expectedException);
            }
            List<Integer> ids = ms.insertMail(Arrays.asList(
                    bean,
                    bean,
                    bean,
                    bean
            ));

            List<MailBean> list = ms.retrieveMailByFolder(bean.getFolderName());
            List<String> folders = new ArrayList<>();
            if (list.size() != 4) {
                Assert.fail("size not 4");
            }
            for (MailBean resultBean : list) {
                if (!resultBean.getFolderName().equals(bean.getFolderName())) {
                    Assert.fail("Folder not the same as queried");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MailStorageModuleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Before
    public void seedDatabase() {
        final String sqlFile = loadString("database/JAG_Create.sql");
        try (Connection connection = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/test?zeroDateTimeBehavior=CONVERT_TO_NULL",
                config.getDatabaseUserName(),
                config.getDatabasePassword());) {
            connection.prepareStatement("USE " + config.getDatabaseName()).execute();
            for (String statement : splitStatements(new StringReader(sqlFile), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    private String loadString(String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path)) {
            Scanner scanner = new Scanner(inputStream);
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input Stream");
        }
    }

    private List<String> splitStatements(Reader reader, String stmtDelimiter) {
        BufferedReader buffReader = new BufferedReader(reader);
        StringBuilder sqlStmnt = new StringBuilder();
        List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = buffReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStmnt.append(line);
                if (line.endsWith(stmtDelimiter)) {
                    statements.add(sqlStmnt.toString());
                    sqlStmnt.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() throws IOException {
        return Arrays.asList(new Object[][]{
            {//VALID BEANS ================================================
                new MailBean(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>",
                Arrays.asList(new AttachmentBean[]{
                    new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))),
                    new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
                }),
                Arrays.asList(new AttachmentBean[]{
                    new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))),
                    new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
                }),
                MessageType.REPLY,
                new Date(),
                new Date(),
                "DELETED",
                EmailPriority.PRIORITY_NORMAL), null},
            {
                //BEAN 2
                new MailBean(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>",
                Arrays.asList(new AttachmentBean[]{
                    //new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))),
                    new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
                }),
                Arrays.asList(new AttachmentBean[]{
                    new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))), //new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
                }),
                MessageType.REPLY,
                new Date(),
                new Date(),
                "JUNK",
                EmailPriority.PRIORITY_NORMAL), null},
            //BEAN 3
            {
                new MailBean(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                "JUNK",
                null),
                null},
            { // INVALID MAIL BEANS ==========================================================
                new MailBean(
                null,
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>",
                Arrays.asList(new AttachmentBean[]{
                    new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))),
                    new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
                }),
                Arrays.asList(new AttachmentBean[]{
                    new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))),
                    new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
                }),
                MessageType.REPLY,
                new Date(),
                new Date(),
                "UNCATEGORIZED",
                EmailPriority.PRIORITY_NORMAL),
                IllegalArgumentException.class},});
    }
}
