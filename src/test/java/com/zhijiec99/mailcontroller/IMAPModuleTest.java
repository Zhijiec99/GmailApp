/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.mailcontroller;
import com.zhijiec99.configurationcontroller.GMailConfig;
import com.zhijiec99.beans.MailBean;
import com.zhijiec99.beans.UserBean;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.*;

/**
 *
 * @author Zhijie Cao
 */
public class IMAPModuleTest {

    static GMailConfig conf;

    @BeforeClass
    public static void init() {
        try {
            conf = new GMailConfig("./src/test/resources/receive_conf");
        } catch (IOException ex) {
            Logger.getLogger(IMAPModuleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//TESTING OF IMAPMODULE IS DONE IN SMTP MODULE TEST
//    @Test
//    public  void testReceive() {
//       IMAPModule imapModule = new IMAPModule(conf);
//       List<MailBean> list = imapModule.receiveMail();
//    }
}
