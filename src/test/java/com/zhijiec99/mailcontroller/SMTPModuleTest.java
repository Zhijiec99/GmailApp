/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.mailcontroller;

import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.configurationcontroller.GMailConfig;
import com.zhijiec99.beans.AttachmentBean;
import com.zhijiec99.data.EmailPriority;
import com.zhijiec99.beans.MailBean;
import com.zhijiec99.data.MessageType;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jodd.mail.EmailAddress;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

/**
 *
 * @author Zhijie Cao
 */
//@RunWith(Parameterized.class)
public class SMTPModuleTest {

    private static FXPropertiesBean receiveConf;
    private static FXPropertiesBean otherConf;
    private static FXPropertiesBean sendConf;

    @BeforeClass
    public static void init() {
        try {
            GMailConfig receiveGmailConf = new GMailConfig("./src/test/resources/receive_conf");
            receiveConf = new FXPropertiesBean(
                    receiveGmailConf.getUserName(),
                receiveGmailConf.getUserEmail(),
                receiveGmailConf.getUserPassword(),
                receiveGmailConf.getImapServerUrl(),
                receiveGmailConf.getSmtpServerUrl(),
                receiveGmailConf.getImapServerPort(), 
                receiveGmailConf.getSmtpServerPort(), 
                receiveGmailConf.getMysqlDatabaseURL(), 
                receiveGmailConf.getMysqlDatabasePort(), 
                receiveGmailConf.getDatabaseName(), 
                receiveGmailConf.getDatabaseUserName(), 
                receiveGmailConf.getDatabasePassword());
            GMailConfig sendGmailConf = new GMailConfig("src/test/resources/send_conf");
            sendConf = new FXPropertiesBean(sendGmailConf.getUserName(),
                sendGmailConf.getUserEmail(),
                sendGmailConf.getUserPassword(),
                sendGmailConf.getImapServerUrl(),
                sendGmailConf.getSmtpServerUrl(),
                sendGmailConf.getImapServerPort(), 
                sendGmailConf.getSmtpServerPort(), 
                sendGmailConf.getMysqlDatabaseURL(), 
                sendGmailConf.getMysqlDatabasePort(), 
                sendGmailConf.getDatabaseName(), 
                sendGmailConf.getDatabaseUserName(), 
                sendGmailConf.getDatabasePassword());
            GMailConfig otherGmailConf = new GMailConfig("./src/test/resources/other_conf");
             otherConf = new FXPropertiesBean(otherGmailConf.getUserName(),
                otherGmailConf.getUserEmail(),
                otherGmailConf.getUserPassword(),
                otherGmailConf.getImapServerUrl(),
                otherGmailConf.getSmtpServerUrl(),
                otherGmailConf.getImapServerPort(), 
                otherGmailConf.getSmtpServerPort(), 
                otherGmailConf.getMysqlDatabaseURL(), 
                otherGmailConf.getMysqlDatabasePort(), 
                otherGmailConf.getDatabaseName(), 
                otherGmailConf.getDatabaseUserName(), 
                otherGmailConf.getDatabasePassword());
        } catch (IOException ex) {
            Logger.getLogger(SMTPModuleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static MailBean[] data() throws IOException {
        return new MailBean[]{
            MailBeanGenerator("Test1", "textMessage", "<h1> Hello,<h1> "
            + " <p style=\"font-size:10px;color:red;\"> Teata <p> "
            + "<br> <a href=\"https://www.youtube.com/watch?v=Gs069dndIYk\"> click me </a>"
            + "<br> <img onclick=\"alert(\"test\");\" src=\"cid:meme.jpeg\"> ",
            Arrays.asList(new AttachmentBean[]{
                //new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))),
                new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
            }),
            Arrays.asList(new AttachmentBean[]{
                new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))), //new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
            })
            ),//null subject
            MailBeanGenerator(null, "textMessage", "<h1> Hello,<h1> "
            + " <p style=\"font-size:10px;color:red;\"> Test2 <p> "
            + "<br> <a href=\"https://www.youtube.com/watch?v=Gs069dndIYk\"> click me </a>"
            + "<br> <img onclick=\"alert(\"test\");\" src=\"cid:meme.jpeg\"> ",
            Arrays.asList(new AttachmentBean[]{
                //new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))),
                new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
            }),
            Arrays.asList(new AttachmentBean[]{
                new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))), //new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
            })
            ),//null htmlMessage
            MailBeanGenerator("Test3", "textMessage", "",
            Arrays.asList(new AttachmentBean[]{
                //new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))),
                new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
            }),
            Arrays.asList(new AttachmentBean[]{
                new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))), //new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
            })
            ),//null textMessage
            MailBeanGenerator("Test4", "", "<h1> Hello,<h1> "
            + " <p style=\"font-size:10px;color:red;\"> This is a paragraph <p> "
            + "<br> <a href=\"https://www.youtube.com/watch?v=Gs069dndIYk\"> click me </a>"
            + "<br> <img onclick=\"alert(\"test\");\" src=\"cid:meme.jpeg\"> ",
            Arrays.asList(new AttachmentBean[]{
                //new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))),
                new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
            }),
            Arrays.asList(new AttachmentBean[]{
                new AttachmentBean("meme.jpeg", Files.readAllBytes(Paths.get("./src/test/resources/meme.jpeg"))), //new AttachmentBean("attachment2.txt", Files.readAllBytes(Paths.get("./src/test/resources/someTextFile.txt")))
            })
            ),//null empty attachmentBeans
            MailBeanGenerator("Test5", "textMessage", "<h1> Hello,<h1> "
            + " <p style=\"font-size:10px;color:red;\"> This is a paragraph <p> "
            + "<br> <a href=\"https://www.youtube.com/watch?v=Gs069dndIYk\"> click me </a>"
            + "<br> <img onclick=\"alert(\"test\");\" src=\"cid:meme.jpeg\"> ",
            Arrays.asList(new AttachmentBean[]{
                null
            }),
            Arrays.asList(new AttachmentBean[]{
                null
            })
            ),//null attachmentBeans
            MailBeanGenerator("Test6", "textMessage", "<h1> Hello,<h1> "
            + " <p style=\"font-size:10px;color:red;\"> This is a paragraph <p> "
            + "<br> <a href=\"https://www.youtube.com/watch?v=Gs069dndIYk\"> click me </a>"
            + "<br> <img onclick=\"alert(\"test\");\" src=\"cid:meme.jpeg\"> ",
            null,
            null
            )

        };
    }

    /**
     * Test of sendEmail method, of class SMTPModule.
     */
    @Test
    public void testSendEmail() throws IOException {
        SMTPModule instance = new SMTPModule(sendConf);
        IMAPModule imapReceive = new IMAPModule(receiveConf);
        for (MailBean mBean : data()) {
            instance.sendEmail(mBean);
        }
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SMTPModuleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<MailBean> mail = imapReceive.receiveMail();
        for (MailBean mBean : data()) {
            boolean fail = false;
            for (MailBean receivedmailBean : mail) {
                //this will verify to, from, cc, subject, htmlMessage and TextMessage
                if (receivedmailBean.equals(mBean)) {
                     fail = true;
                }
            }
            if(!fail){
                fail("did not find the corresponding MailBean");
            }
        }
        //assertEquals(expectedResult, receivedMail != null);
    }

    private static MailBean MailBeanGenerator(
            String subject,
            String textMessage,
            String htmlMessage,
            List<AttachmentBean> attachment,
            List<AttachmentBean> embeddedAttachment) {
        return new MailBean(
                new EmailAddress("send", "send.1633068@gmail.com"),
                new EmailAddress[]{new EmailAddress("receive", "receive.1633068@gmail.com")},
                new EmailAddress[]{new EmailAddress("other", "other.1633068@gmail.com")},
                null,
                subject,
                textMessage,
                htmlMessage,
                attachment,
                embeddedAttachment,
                MessageType.REPLY,
                null,
                null,
                "",
                EmailPriority.PRIORITY_NORMAL);
    }

}
