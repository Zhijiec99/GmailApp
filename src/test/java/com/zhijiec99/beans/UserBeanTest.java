/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.beans;

import com.zhijiec99.beans.UserBean;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

/**
 *
 * @author Zhijie Cao
 */
@RunWith(Parameterized.class)
public class UserBeanTest {

    @Parameter(0)
    public UserBean userBean1;

    @Parameter(1)
    public UserBean userBean2;

    @Parameter(2)
    public boolean expectedResult;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {new UserBean("Zhi", "zhijiec99@hotmail.com", "fakePassw0rD!*"),
                new UserBean("asd", "zhijiec99@hotmail.com", "fakePassw0rD!*"), false},
            {new UserBean("Zhi", "zhijiec99@hotmail.com", "fakePassw0rD!*"),
                new UserBean("Zhi", "asdakj@hotmail.com", "fakePassw0rD!*"), false},
            {new UserBean("Zhi", "zhijiec99@hotmail.com", "fakePassw0rD!*"),
                new UserBean("Zhi", "zhijiec99@hotmail.com", "asds*"), false},
            {new UserBean("Zhi", "zhijiec99@hotmail.com", "fakePassw0rD!*"),
                new UserBean("Zhi", "zhijiec99@hotmail.com", "fakePassw0rD!*"), true},            
        });
    }

    /**
     * Test of equals method, of class UserBean.
     */
    @Test
    public void testEquals() {
        assertTrue(expectedResult == userBean1.equals(userBean2));
    }
    
    @Test
    public void testHashCode(){
        assertTrue(expectedResult == (userBean1.hashCode() == userBean2.hashCode()) );
    }

}
