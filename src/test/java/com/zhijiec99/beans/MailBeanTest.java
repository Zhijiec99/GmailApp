/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.beans;

import com.zhijiec99.data.EmailPriority;
import com.zhijiec99.beans.AttachmentBean;
import com.zhijiec99.data.MessageType;
import com.zhijiec99.beans.MailBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import jodd.mail.EmailAddress;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

/**
 *
 * @author Zhijie Cao
 */
@RunWith(Parameterized.class)
public class MailBeanTest {

    @Parameter(0)
    public MailBean mailBean1;

    @Parameter(1)
    public MailBean mailBean2;

    @Parameter(2)
    public boolean expectedResult;

    /**
     * Test of equals method, of class MailBean.
     */
    @Test
    public void testEquals() {
        assertEquals(expectedResult, mailBean1.equals(mailBean2));
    }

    /**
     * Test of hashCode method, of class MailBean.
     */
    @Test
    public void testHashCode() {
        assertEquals(expectedResult, mailBean1.hashCode() == mailBean2.hashCode());
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            //TWO IDENTICAL MAILBEAN
            {MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                true
            },
            //Different cc
            {MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Ngasduyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                false
            },
            //different from
            {MailBeanGenerator(
                new EmailAddress("Zhi", "NOTZHI@hotmail.com"),
                new EmailAddress[]{},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                false
            }, //DIFFERENT TO
            {MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pescaidoioe@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                false
            },
            //null CC
            {MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pescaidoioe@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                false
            },
            //null CC
            {MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pescaidoioe@hotmail.com"),},
                null,
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                false
            },//different subject
            {MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                null,
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                false
            },//different subject
            {MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLasdasddaldmaslkdamldkaslkmlO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                false
            }, //different textMessage
            {MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE:adssdad Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                false
            }, //different html message
            {MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                null
                ), MailBeanGenerator(
                new EmailAddress("Zhi", "Zhi@hotmail.com"),
                new EmailAddress[]{
                    new EmailAddress("Jon", "Pesce@hotmail.com"),},
                new EmailAddress[]{
                    new EmailAddress("Quan", "Nguyen@hotmail.com"),
                    new EmailAddress("Nick", "Apanian@hotmail.com"),},
                "SUBJECT: HELLO",
                "TEXTMESSAGE: Hello students",
                "<p> THIS IS AN HTML MESSAGE <p>"
                ),
                false
            }

        });
    }

    private static MailBean MailBeanGenerator(
            EmailAddress from,
            EmailAddress[] to,
            EmailAddress[] cc,
            String subject,
            String textMessage,
            String htmlMessage) {
        return new MailBean(
                from,
                to,
                cc,
                null,
                subject,
                textMessage,
                htmlMessage,
                Arrays.asList(new AttachmentBean[]{
            new AttachmentBean("attachment1.jpg", "someImage".getBytes()),
            new AttachmentBean("attachment2.txt", "Content".getBytes())
        }),
                Arrays.asList(new AttachmentBean[]{
            new AttachmentBean("embeddedAttachment.jpg", "someImage".getBytes()),
            new AttachmentBean("embeddedAttachment2.txt", "Content".getBytes())
        }),
                MessageType.REPLY,
                new Date(),
                new Date(),
                "",
                EmailPriority.PRIORITY_NORMAL);
    }
}
