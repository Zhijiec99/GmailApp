/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.beans;

import com.zhijiec99.data.EmailPriority;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

/**
 *
 * @author Zhijie Cao
 */
@RunWith(Parameterized.class)
public class EmailPriorityTest {

    @Parameter(0)
    public int priorityValue;

    @Parameter(1)
    public EmailPriority priority;

    @Parameter(2)
    public Class<? extends Exception> expectedException;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {1, EmailPriority.PRIORITY_HIGHEST, null},
            {2, EmailPriority.PRIORITY_HIGH, null},
            {3, EmailPriority.PRIORITY_NORMAL, null},
            {4, EmailPriority.PRIORITY_LOW, null},
            {5, EmailPriority.PRIORITY_LOWEST, null},
            //{99, null, IllegalArgumentException.class}
        });
    }

    /**
     * Test of getPriority method, of class EmailPriority.
     */
    @Test
    public void testGetPriority() {
        if (expectedException != null) {
            thrown.expect(expectedException);
        }

        assertEquals(priority, EmailPriority.getPriority(priorityValue));
    }

}
