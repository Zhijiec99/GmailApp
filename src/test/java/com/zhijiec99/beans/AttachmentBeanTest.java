/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.beans;

import com.zhijiec99.beans.AttachmentBean;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;
import org.junit.*;
import org.junit.experimental.runners.Enclosed;
import org.junit.rules.ExpectedException;
import org.junit.runner.*;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

/**
 *
 * @author Zhijie Cao
 */
@RunWith(Enclosed.class)
public class AttachmentBeanTest {

    public static class AttachmentBeanEqualsTest {

        /**
         * Test of hashCode method, of class AttachmentBean.
         */
        @Test
        public void testHashCode() {
            AttachmentBean bean1 = new AttachmentBean("test", "test".getBytes());
            AttachmentBean bean2 = new AttachmentBean("test", "test".getBytes());
            Assert.assertEquals(true, bean1.hashCode() == bean2.hashCode());
        }

        @Test
        public void testHashCodeUnequalName() {
            AttachmentBean bean1 = new AttachmentBean("test", "test".getBytes());
            AttachmentBean bean2 = new AttachmentBean("test2", "test".getBytes());
            Assert.assertEquals(false, bean1.hashCode() == bean2.hashCode());
        }

        @Test
        public void testHashCodeUnEqualContent() {
            AttachmentBean bean1 = new AttachmentBean("test", "test".getBytes());
            AttachmentBean bean2 = new AttachmentBean("test", "test2".getBytes());
            Assert.assertEquals(false, bean1.hashCode() == bean2.hashCode());
        }

        /**
         * Test of equals method, of class AttachmentBean.
         */
        @Test
        public void testEqualsEqual() {
            AttachmentBean bean1 = new AttachmentBean("test", "test".getBytes());
            AttachmentBean bean2 = new AttachmentBean("test", "test".getBytes());
            Assert.assertEquals(true, bean1.equals(bean2));
        }

        @Test
        public void testEqualsUnequalName() {
            AttachmentBean bean1 = new AttachmentBean("test", "test".getBytes());
            AttachmentBean bean2 = new AttachmentBean("tes2t", "test".getBytes());
            Assert.assertEquals(false, bean1.equals(bean2));
        }

        @Test
        public void testEqualsUnequalContent() {
            AttachmentBean bean1 = new AttachmentBean("test", "test".getBytes());
            AttachmentBean bean2 = new AttachmentBean("test", "tes2t".getBytes());
            Assert.assertEquals(false, bean1.equals(bean2));
        }
    }

    @RunWith(Parameterized.class)
    public static class AttachmentBeanConstructorTest {

        private static Random random;

        @Parameters
        public static Collection<Object[]> data() {
            random = new Random();
            return Arrays.asList(new Object[][]{
                {"image.png", null, null, null, IllegalArgumentException.class, false},
                {null, byteArrayGenerator(20), null, null, IllegalArgumentException.class, false},
                {"image.png", byteArrayGenerator(20), null, null, null, true}
            });
        }

        @Parameter(0)
        public String name;

        @Parameter(1)
        public byte[] content;

        @Parameter(2)
        public String name2;

        @Parameter(3)
        public byte[] content2;

        @Parameter(4)
        public Class<? extends Exception> expectedException;

        @Parameter(5)
        public boolean expectedResult;

        @Rule
        public ExpectedException thrown = ExpectedException.none();

        private static byte[] byteArrayGenerator(int size) {
            byte[] randBytes = new byte[size];
            random.nextBytes(randBytes);
            return randBytes;
        }

        @Test
        public void testConstructor() {
            if (expectedException != null) {
                thrown.expect(expectedException);
            }
            AttachmentBean attachmentBean = new AttachmentBean(name, content);
            Assert.assertNotNull(attachmentBean);
        }
    }
}
