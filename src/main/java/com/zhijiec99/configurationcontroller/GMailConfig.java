/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.configurationcontroller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class is used to retrieve configuration information from
 *
 * @author Zhijie Cao
 */
public class GMailConfig {

    private Properties properties;
    private String filePath;

    private String userName;
    private String userEmail;
    private String userPassword;

    private String ImapServerUrl;
    private String SmtpServerUrl;
    private int ImapServerPort;
    private int SmtpServerPort;
    //TODO: database information;
    private String mysqlDatabaseURL;
    private String databaseName;
    private int mysqlDatabasePort;
    private String databaseUserName;
    private String databasePassword;

    public GMailConfig(String filePath) throws FileNotFoundException, IOException {
        this.properties = new Properties();
        this.filePath = filePath;
        File file = new File(filePath);
        InputStream stream = new FileInputStream(file);
        this.properties = new Properties();
        properties.load(stream);
        if (!loadProperties()) {
            throw new IllegalArgumentException(
                    "One of the value is invalid."
                    + "please check the configuration file.");
        }
    }

    /**
     * Loads the properties and assign them to variables
     *
     * @return true if all variables set properly, false if one or more
     * properties is null
     */
    public final boolean loadProperties() {

        this.userName = properties.getProperty("userName");
        this.userEmail = properties.getProperty("userEmail");
        this.userPassword = properties.getProperty("userPassword");
        this.ImapServerUrl = properties.getProperty("ImapServerUrl");
        this.SmtpServerUrl = properties.getProperty("SmtpServerUrl");
        this.ImapServerPort = Integer.parseInt(properties.getProperty("ImapServerPort", "993"));
        this.SmtpServerPort = Integer.parseInt(properties.getProperty("SmtpServerPort", "465"));

        this.mysqlDatabaseURL = properties.getProperty("mysqlDatabaseURL");
        this.mysqlDatabasePort = Integer.parseInt(properties.getProperty("mysqlDatabasePort", "3306"));
        this.databaseName = properties.getProperty("databaseName");
        this.databaseUserName = properties.getProperty("databaseUserName");
        this.databasePassword = properties.getProperty("databasePassword");

        return !(userName == null
                || userEmail == null
                || userPassword == null
                || ImapServerUrl == null
                || SmtpServerUrl == null
                || mysqlDatabaseURL == null
                || databaseName == null
                || databaseUserName == null
                || databasePassword == null);
    }

    /**
     * gets the userName
     *
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     * gets the userEmail
     *
     * @return
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * gets the password
     *
     * @return
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * gets the imapServerUrl
     *
     * @return
     */
    public String getImapServerUrl() {
        return ImapServerUrl;
    }

    /**
     * gets the smtpserverUrl
     *
     * @return
     */
    public String getSmtpServerUrl() {
        return SmtpServerUrl;
    }

    /**
     * gets the imapPort
     *
     * @return
     */
    public int getImapServerPort() {
        return ImapServerPort;
    }

    /**
     * gets the smtpPort
     *
     * @return
     */
    public int getSmtpServerPort() {
        return SmtpServerPort;
    }

    public String getMysqlDatabaseURL() {
        //jdbc:mysql://localhost:3306/?user=root
        //return "jdbc:mysql://" +this.mysqlDatabaseURL + ":" + getMysqlDatabasePort() + "/?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
        return this.mysqlDatabaseURL;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public int getMysqlDatabasePort() {
        return mysqlDatabasePort;
    }

    public String getDatabaseUserName() {
        return databaseUserName;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }

}
