/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.beans;

import java.util.Objects;
import jodd.mail.RFC2822AddressParser;

/**
 * This is a userBean, it stores the basic information of an user.
 * @author Zhijie Cao
 */
public class UserBean {

    private String userName;
    private String userEmail;
    private String userPassword;
    
    /**
     * Creates an user Bean
     * @param userName
     * @param userEmail
     * @param userPassword 
     */
    public UserBean(String userName, String userEmail, String userPassword) {
        if(userName == null)
            throw new IllegalArgumentException("UserName cannot be null");
        if(userPassword == null)
            throw new IllegalArgumentException("UserPassword cannot be null");
        if(userEmail == null)
            throw new IllegalArgumentException("UserEmail cannot be null");
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
    }
    /**
     * gets userName
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }
    /**
     * gets userEmail
     * @return the userEmail
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * gets userPassword
     * @return userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * Compares if the two objects are the equal. They are equal if they have
     * the same username, email and password.
     *
     * @param obj
     * @return if the two objects are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserBean other = (UserBean) obj;
        
        return userName.equals(other.userName) 
                && userPassword.equals(other.userPassword)
                && userEmail.equals(other.userEmail);
    }
    /**
     * the Computed hash
     * @return the computed Hash
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.userName);
        hash = 41 * hash + Objects.hashCode(this.userEmail);
        hash = 41 * hash + Objects.hashCode(this.userPassword);
        return hash;
    }
}
