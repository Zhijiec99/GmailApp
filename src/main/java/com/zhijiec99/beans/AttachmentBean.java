/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.beans;

import java.util.Arrays;

/**
 * AttachmentBean to store Attachments
 * @author Zhijie Cao
 */
public class AttachmentBean extends Bean {

    private String name;
    private byte[] content;
    
    /**
     * Constructor
     * @param name, the name of the file
     * @param content, the content of the file
     */
    public AttachmentBean(String name, byte[] content) {
        if(name == null)
            throw new IllegalArgumentException("The name cannot be null");
        if(content == null)
            throw new IllegalArgumentException("The content cannot be null");
        this.name = name;
        this.content = content;
    }
    
    /**
     * gets the NAME
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * gets the Content
     * @return 
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Computes the hashCode
     * @return computed hashCode
     */
    @Override
    public int hashCode() {
        return Arrays.hashCode(content) + name.hashCode();
    }
    
    /**
     * Two objects are equal if they have the same name and content
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AttachmentBean)) {
            return false;
        }
        AttachmentBean attachmentBean = ((AttachmentBean) obj);
        if (attachmentBean.name == null ? this.name != null : !attachmentBean.name.equals(this.name)) {
            return false;
        }
        return Arrays.equals(attachmentBean.content, this.content);
    }
}
