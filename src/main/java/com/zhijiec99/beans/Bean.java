/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.beans;

import java.io.Serializable;

/**
 * Parent class for bean objects
 * @author Zhijie Cao
 */
public abstract class Bean implements Serializable{

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object obj);

}
