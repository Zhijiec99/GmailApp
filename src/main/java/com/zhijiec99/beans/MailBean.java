/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.beans;

import com.zhijiec99.data.EmailPriority;
import com.zhijiec99.data.MessageType;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import jodd.mail.EmailAddress;

/**
 * This is the Bean for the email.
 *
 * @author Zhijie Cao
 */
public class MailBean extends Bean {

    //TODO: REFER TO EMAILS
    private EmailAddress from;
    private EmailAddress[] to;
    private EmailAddress[] cc;
    private EmailAddress[] bcc;
    private String subject;
    private String textMessage;
    private String htmlMessage;
    private List<AttachmentBean> attachments;
    private List<AttachmentBean> embeddedAttachments;
    private MessageType messageType;
    private Date sentTime;
    private Date receivedTime;
    private String folderName;
    private EmailPriority priority;
    private int emailId;

    /**
     * Overloaded Constructor with only ID;
     * @param emailId 
     */
    public MailBean(int emailId) {
        this.emailId = emailId;
    }
    
    /**
     * Controller for the MailBean class.
     * @param from
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param attachments
     * @param embeddedAttachments
     * @param messageType
     * @param sentTime
     * @param receivedTime
     * @param folderName
     * @param priority
     * @param id 
     */
    public MailBean(
            EmailAddress from,
            EmailAddress[] to,
            EmailAddress[] cc,
            EmailAddress[] bcc,
            String subject,
            String textMessage,
            String htmlMessage,
            List<AttachmentBean> attachments,
            List<AttachmentBean> embeddedAttachments,
            MessageType messageType,
            Date sentTime,
            Date receivedTime,
            String folderName,
            EmailPriority priority,
            int id) {
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.textMessage = textMessage;
        this.htmlMessage = htmlMessage;
        this.attachments = attachments;
        this.embeddedAttachments = embeddedAttachments;
        this.messageType = messageType;
        this.sentTime = sentTime;
        this.receivedTime = receivedTime;
        this.folderName = folderName;
        this.priority = priority;
        this.emailId = id;
    }
    
    /**
     * Overloaded constructor without an ID.
     * @param from
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param attachments
     * @param embeddedAttachments
     * @param messageType
     * @param sentTime
     * @param receivedTime
     * @param folderName
     * @param priority 
     */
    public MailBean(
            EmailAddress from,
            EmailAddress[] to,
            EmailAddress[] cc,
            EmailAddress[] bcc,
            String subject,
            String textMessage,
            String htmlMessage,
            List<AttachmentBean> attachments,
            List<AttachmentBean> embeddedAttachments,
            MessageType messageType,
            Date sentTime,
            Date receivedTime,
            String folderName,
            EmailPriority priority){
        this(from, to, cc, bcc, subject, textMessage, htmlMessage, attachments, embeddedAttachments, messageType, sentTime, receivedTime, folderName, priority, 0);
    }

    /**
     * Get the emailId
     * @return
     */
    public int getEmailId() {
        return emailId;
    }
    
    
    /**
     * Get the FROM
     * @return 
     */
    public EmailAddress getFrom() {
        return from;
    }

    /**
     * Get the TO
     * @return 
     */
    public EmailAddress[] getTo() {
        return to;
    }

    /**
     * Get the CC
     * @return 
     */
    public EmailAddress[] getCc() {
        return cc;
    }

    /**
     * Get the BCC
     * @return 
     */
    public EmailAddress[] getBcc() {
        return bcc;
    }

    /**
     * Get the subject
     * @return 
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Get the MESSAGE
     * @return 
     */
    public String getTextMessage() {
        return textMessage;
    }

    /**
     * Get the HTMLMESSAGE
     * @return 
     */
    public String getHtmlMessage() {
        return htmlMessage;
    }

    /**
     * Get the attachments
     * @return 
     */
    public List<AttachmentBean> getAttachments() {
        return attachments;
    }

    /**
     * Get the embeddedAttachments
     * @return 
     */
    public List<AttachmentBean> getEmbeddedAttachments() {
        return embeddedAttachments;
    }

    /**
     * Get the messageType
     * @return 
     */
    public MessageType getMessageType() {
        return messageType;
    }

    /**
     * Get the sentTime
     * @return 
     */
    public Date getSentTime() {
        return sentTime;
    }

    /**
     * Get the receiveTime
     * @return 
     */
    public Date getReceivedTime() {
        return receivedTime;
    }

    /**
     * Get the folderName
     * @return 
     */
    public String getFolderName() {
        return folderName;
    }

    /**
     * Get the Priority
     * @return 
     */
    public EmailPriority getPriority() {
        return priority;
    }

    /**
     * Two mailBean objects are equal if they have the same from, to, cc,
     * subject, textMessage and HTMLMessage.
     *
     *
     * @param obj
     * @return if the two objects are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MailBean)) {
            return false;
        }
        
        if (obj == null) {
            return false;
        }
        
        MailBean mailBean = (MailBean) obj;

        if (mailBean.to == null) {
            if (this.to != null) {
                return false;
            }
        }

        if (this.to == null) {
            return false;
        }

        if (mailBean.to.length != this.to.length) {
            return false;
        }

        for (int i = 0; i < mailBean.to.length; i++) {
            if (!mailBean.to[i].getEmail().equals(this.to[i].getEmail())) {
                return false;
            }
        }

        if (mailBean.cc == null) {
            if (this.cc != null) {
                return false;
            }
        }
        if (this.cc == null) {
            return false;
        }
        if (mailBean.cc.length != this.cc.length) {
            return false;
        }
        for (int i = 0; i < mailBean.cc.length; i++) {
            if (!mailBean.cc[i].getEmail().equals(this.cc[i].getEmail())) {
                return false;
            }
        }

        return ((this.from.getEmail()) == null ? mailBean.from.getEmail() == null : this.from.getEmail().equals(mailBean.from.getEmail())
                && (this.subject == null ? mailBean.subject == null : this.subject.equals(mailBean.subject))
                && (this.textMessage == null ? mailBean.textMessage == null : this.textMessage.equals(mailBean.textMessage))
                && (this.htmlMessage == null ? mailBean.htmlMessage == null : this.htmlMessage.equals(mailBean.htmlMessage)));
    }
    
    /**
     * Computes the hashCode
     * @return the computed hashCode
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.from.getEmail());
        if( this.to != null)
            for(EmailAddress email : this.to){
                if(email != null)
                    hash = 47 * hash + email.getEmail().hashCode();
            }
        if(this.cc != null)
            for(EmailAddress email : this.cc){
                if(email != null)
                    hash = 47 * hash + email.getEmail().hashCode();
            }
        hash = 47 * hash + Objects.hashCode(this.subject);
        hash = 47 * hash + Objects.hashCode(this.textMessage);
        hash = 47 * hash + Objects.hashCode(this.htmlMessage);
        return hash;
    }

}
