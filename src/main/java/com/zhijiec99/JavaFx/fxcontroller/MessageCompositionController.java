/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.JavaFx.fxcontroller;

/**
 * Sample Skeleton for 'messageComposition.fxml' Controller Class
 */
import com.zhijiec99.JavaFx.fxbeans.FXFolderBean;
import com.zhijiec99.JavaFx.fxbeans.FXMailBean;
import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.JavaFx.manager.AttachmentManager;
import com.zhijiec99.JavaFx.manager.FXBeanManager;
import com.zhijiec99.JavaFx.manager.PropertiesManager;
import com.zhijiec99.beans.AttachmentBean;
import com.zhijiec99.data.MessageType;
import com.zhijiec99.databasecontroller.FolderStorageModule;
import com.zhijiec99.databasecontroller.MailStorageModule;
import com.zhijiec99.mailcontroller.SMTPModule;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Stage;
import jodd.mail.EmailAddress;
import jodd.mail.RFC2822AddressParser;

public class MessageCompositionController {

    private PropertiesManager manager;
    private AttachmentManager attachmentManager;
    private FXPropertiesBean properties;
    private FolderStorageModule folderStorageModule;
    private MailStorageModule mailStorageModule;
    private List<FXFolderBean> folders;
    private List<AttachmentBean> attachments;
    private FXBeanManager fxBeanManager;
    private FXMailBean mailBean;
    private SMTPModule smtp;
    private HTMLEditor htmlEditor;
    private TextArea textAreaEditor;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="toField"
    private TextField toField; // Value injected by FXMLLoader

    @FXML // fx:id="ccField"
    private TextField ccField; // Value injected by FXMLLoader

    @FXML // fx:id="bccField"
    private TextField bccField; // Value injected by FXMLLoader

    @FXML // fx:id="subjectField"
    private TextField subjectField; // Value injected by FXMLLoader

    @FXML // fx:id="isHTMLEditor"
    private CheckBox isHTMLEditor; // Value injected by FXMLLoader

    @FXML // fx:id="editor"
    private BorderPane editor; // Value injected by FXMLLoader

    @FXML // fx:id="attachmentBox"
    private ListView attachmentBox; // Value injected by FXMLLoader

    public MessageCompositionController() {
        try {
            this.attachmentManager = new AttachmentManager();
            this.manager = new PropertiesManager();
            this.properties = manager.loadProperties();
            this.mailStorageModule = new MailStorageModule(properties);
            this.folderStorageModule = new FolderStorageModule(properties);
            this.fxBeanManager = new FXBeanManager();
            this.smtp = new SMTPModule(properties);
            this.attachments = new ArrayList<>();
            this.mailBean = new FXMailBean();
            //insert mock data
        } catch (SQLException | IOException ex) {
            alertMessage("Error", "An error occured during Initialization", ex.getMessage());
        }
    }

    /**
     * called when a new attachment is added
     *
     * @param event
     */
    @FXML
    void onAddNewAttachment(ActionEvent event) {
        try {
            AttachmentBean att = attachmentManager.loadAttachment(attachmentBox.getScene());
            attachments.add(att);
            Button b = new Button(att.getName());
            attachmentBox.getItems().add(b);
            
            File f = new File("C:/Temp/"+att.getName());
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(att.getContent());
            
            b.setOnAction(e -> {
                attachments.remove(att);
                attachmentBox.getItems().remove(b);
            });
        } catch (IOException ex) {
            alertMessage("Error", "An error occured while trying to attach the file", "Please try another file");
        }
    }
    
    @FXML
    void onInsertNewAttachment(ActionEvent event){
         try {
            AttachmentBean att = attachmentManager.loadAttachment(attachmentBox.getScene());
            attachments.add(att);
            Button b = new Button(att.getName());
            attachmentBox.getItems().add(b);
            
            File f = new File("C:/Temp/"+att.getName());
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(att.getContent());
            
            b.setOnAction(e -> {
                attachments.remove(att);
                attachmentBox.getItems().remove(b);
            });
            
            htmlEditor.setHtmlText(htmlEditor.getHtmlText() + " <img src=\"file:///C:/Temp/" + att.getName() + "\"/>");
        } catch (IOException ex) {
            alertMessage("Error", "An error occured while trying to attach the file", "Please try another file");
        }
    }

    /**
     * Called when you press cancel
     *
     * @param event
     */
    @FXML
    void onCancel(ActionEvent event) {
        ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Called when you toggle the editor
     *
     * @param event
     */
    @FXML
    void onHTMLEditorToggle(ActionEvent event) {
        if (editor.getChildren().get(0) instanceof HTMLEditor) {
            editor.setCenter(textAreaEditor);
        } else {
            editor.setCenter(htmlEditor);
        }
    }

    /**
     * Called when sent button is pressed
     *
     * @param event
     */
    @FXML
    void onSend(ActionEvent event) {
        try {
            if (!parseInfo()) {
                return;
            }
            if (!mailBean.getTo().isEmpty() || !mailBean.getCc().isEmpty() || !mailBean.getBcc().isEmpty()) {
                alertMessage("", "", mailBean.getEmailId() + " " + mailBean.getCc() + mailBean.getTo() + mailBean.getBcc() + mailBean.getFrom());
                mailStorageModule.insertMail(fxBeanManager.convertFXMailBean(mailBean));
                smtp.sendEmail(fxBeanManager.convertFXMailBean(mailBean));
                alertMessage("Success", "You have successfully sent an email", "");
                ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
            } else {
                alertMessage("Error", "Invalid Data.", "You must have at least one recipient (to, cc or bcc).");
            }
        } catch (SQLException ex) {
            alertMessage("Error", "Message Could not be sent", ex.getMessage());
        }
    }

    /**
     * parses the Data from the UI to the bean
     * @return if it succeeded or not
     */
    private boolean parseInfo() {
        if (!toField.getText().isEmpty()) {
            for (String email : toField.getText().split(" ")) {
                if (checkEmail(email)) {
                    mailBean.getTo().add(new EmailAddress("", email));
                } else {
                    alertMessage("Error", "You entered an invalid email", "Email: <" + email + "> isn't valid.");
                    return false;
                }
            }
        }
        if (!ccField.getText().isEmpty()) {
            for (String email : ccField.getText().split(" ")) {
                if (checkEmail(email)) {
                    mailBean.getCc().add(new EmailAddress("", email));
                } else {
                    alertMessage("Error", "You entered an invalid email", "Email: <" + email + "> isn't valid.");
                    return false;
                }
            }
        }
        if (!bccField.getText().isEmpty()) {
            for (String email : bccField.getText().split(" ")) {
                if (checkEmail(email)) {
                    mailBean.getBcc().add(new EmailAddress("", email));
                } else {
                    alertMessage("Error", "You entered an invalid email", "Email: <" + email + "> isn't valid.");
                    return false;
                }
            }
        }
        mailBean.setFolderName("SENT");
        mailBean.setFrom(new EmailAddress("", properties.getUserEmail()));
        mailBean.setTextMessage(textAreaEditor.getText());
        mailBean.setHtmlMessage(htmlEditor.getHtmlText().replaceAll("&lt;", "<").replaceAll("&rt;", ">"));
        mailBean.setAttachments(attachments);
        mailBean.setEmbeddedAttachments(attachments);
        return true;
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert toField != null : "fx:id=\"toField\" was not injected: check your FXML file 'messageComposition.fxml'.";
        assert ccField != null : "fx:id=\"ccField\" was not injected: check your FXML file 'messageComposition.fxml'.";
        assert bccField != null : "fx:id=\"bccField\" was not injected: check your FXML file 'messageComposition.fxml'.";
        assert subjectField != null : "fx:id=\"subjectField\" was not injected: check your FXML file 'messageComposition.fxml'.";
        assert isHTMLEditor != null : "fx:id=\"isHTMLEditor\" was not injected: check your FXML file 'messageComposition.fxml'.";
        assert editor != null : "fx:id=\"editor\" was not injected: check your FXML file 'messageComposition.fxml'.";
        assert attachmentBox != null : "fx:id=\"attachmentBox\" was not injected: check your FXML file 'messageComposition.fxml'.";
        this.htmlEditor = (HTMLEditor) editor.getChildren().get(0);
        this.textAreaEditor = new TextArea();
        textAreaEditor.setMinSize(htmlEditor.getMinWidth(), htmlEditor.getMinHeight());
        textAreaEditor.setPrefSize(htmlEditor.getPrefWidth(), htmlEditor.getPrefHeight());
        textAreaEditor.setMaxSize(htmlEditor.getMaxWidth(), htmlEditor.getMaxHeight());
        mailBean.getSubjectProperty().bindBidirectional(subjectField.textProperty());
    }

   /**
    * Initializes Mail with forward
    * @param bean 
    */
    public void setReply(FXMailBean bean) {
        mailBean.setMessageType(MessageType.REPLY);
        String to = "";
        for (EmailAddress email : bean.getTo()) {
            to = email.getEmail() + " ";
        }
        toField.setText(to);

        String cc = "";
        for (EmailAddress email : bean.getCc()) {
            cc = email.getEmail() + " ";
        }
        ccField.setText(cc);

        subjectField.setText("RE: " + bean.getSubject());
        bean.setEmbeddedAttachments(bean.getEmbeddedAttachments());
        htmlEditor.setHtmlText(bean.getHtmlMessage());
        textAreaEditor.setText(bean.getTextMessage());
    }

    /**
     * Initializes mail with forward
     * @param bean 
     */
    public void setForward(FXMailBean bean) {
        mailBean.setMessageType(MessageType.FORWARD);

        subjectField.setText("FW: " + bean.getSubject());
        bean.setEmbeddedAttachments(bean.getEmbeddedAttachments());
        htmlEditor.setHtmlText(bean.getHtmlMessage());
        textAreaEditor.setText(bean.getTextMessage());
    }

    /**
     * Creates an alert Message
     *
     * @param title the title
     * @param header the header
     * @param msg the content
     */
    private void alertMessage(String title, String header, String msg) {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(msg);
        alert.showAndWait();
    }

    /**
     * Use RFC2822AddressParser to validate email address.
     *
     * @param address
     * @return true if valid email, false if not.
     * @author Ken
     */
    protected final boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }

}
