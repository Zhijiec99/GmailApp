/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.JavaFx.fxcontroller;

/**
 * Sample Skeleton for 'configComposition.fxml' Controller Class
 */
import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.JavaFx.manager.DatabaseManager;
import com.zhijiec99.JavaFx.manager.PropertiesManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import jodd.mail.RFC2822AddressParser;

public class ConfigCompositionController {

    private FXPropertiesBean propertiesBean;
    private PropertiesManager manager;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="userName"
    private TextField userName; // Value injected by FXMLLoader

    @FXML // fx:id="userEmail"
    private TextField userEmail; // Value injected by FXMLLoader

    @FXML // fx:id="userPassword"
    private TextField userPassword; // Value injected by FXMLLoader

    @FXML // fx:id="imapServerUrl"
    private TextField imapServerUrl; // Value injected by FXMLLoader

    @FXML // fx:id="imapServerPort"
    private TextField imapServerPort; // Value injected by FXMLLoader

    @FXML // fx:id="smtpServerUrl"
    private TextField smtpServerUrl; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlDatabaseUrl"
    private TextField mysqlDatabaseUrl; // Value injected by FXMLLoader

    @FXML // fx:id="databaseName"
    private TextField databaseName; // Value injected by FXMLLoader

    @FXML // fx:id="mysqlDatabasePort"
    private TextField mysqlDatabasePort; // Value injected by FXMLLoader

    @FXML // fx:id="databaseUserName"
    private TextField databaseUserName; // Value injected by FXMLLoader

    @FXML // fx:id="databasePassword"
    private TextField databasePassword; // Value injected by FXMLLoader

    @FXML // fx:id="smtpServerPort"
    private TextField smtpServerPort; // Value injected by FXMLLoader

    public ConfigCompositionController(){
        try {
            this.manager = new PropertiesManager();
            this.propertiesBean = manager.loadProperties();
        } catch (FileNotFoundException fnfe) {
            this.propertiesBean = new FXPropertiesBean();
        } catch (IOException ex) {
            Logger.getLogger(ConfigCompositionController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Called when you press on the Create button
     *
     * @param event
     */
    @FXML
    void onCreate(ActionEvent event) {
        try {
            if (validFields()) {
                manager.saveProperties(propertiesBean);
                DatabaseManager dm = new DatabaseManager();
                dm.seedDatabase(propertiesBean);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/mailInterface.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root, 1000, 700);
                Stage primaryStage = new Stage();
                primaryStage.setTitle("Mail Interface");
                primaryStage.setScene(scene);
                primaryStage.setResizable(false);
                primaryStage.show();
                ((Stage) ((Node) event.getSource()).getScene().getWindow()).close();
            } else {
                alertMessage("Error", "Invalid Input", "Please make sure all the fields are entered properly (and emails are valid)");
            }
        } catch (SQLException sqlEx){
            alertMessage("Error", sqlEx.getMessage(), "Please try again" );
            Logger.getLogger(ConfigCompositionController.class.getName()).log(Level.SEVERE, null, sqlEx);
        } catch (IOException ex) {
            Logger.getLogger(ConfigCompositionController.class.getName()).log(Level.SEVERE, null, ex);
            alertMessage("Error", "Something went wrong", ex.getMessage());
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert userName != null : "fx:id=\"userName\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert userEmail != null : "fx:id=\"userEmail\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert userPassword != null : "fx:id=\"userPassword\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert imapServerUrl != null : "fx:id=\"imapServerUrl\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert imapServerPort != null : "fx:id=\"imapServerPort\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert smtpServerUrl != null : "fx:id=\"smtpServerUrl\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert mysqlDatabaseUrl != null : "fx:id=\"mysqlDatabaseUrl\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert databaseName != null : "fx:id=\"databaseName\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert mysqlDatabasePort != null : "fx:id=\"mysqlDatabasePort\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert databaseUserName != null : "fx:id=\"databaseUserName\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert databasePassword != null : "fx:id=\"databasePassword\" was not injected: check your FXML file 'configComposition.fxml'.";
        assert smtpServerPort != null : "fx:id=\"smtpServerPort\" was not injected: check your FXML file 'configComposition.fxml'.";


        Bindings.bindBidirectional(userName.textProperty(), propertiesBean.getUserNameProperty());
        Bindings.bindBidirectional(userEmail.textProperty(), propertiesBean.getUserEmailProperty());
        Bindings.bindBidirectional(userPassword.textProperty(), propertiesBean.getUserPasswordProperty());
        Bindings.bindBidirectional(imapServerUrl.textProperty(), propertiesBean.getImapServerUrlProperty());
        Bindings.bindBidirectional(imapServerPort.textProperty(), propertiesBean.getImapServerPortProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(mysqlDatabaseUrl.textProperty(), propertiesBean.getMysqlDatabaseURLProperty());
        Bindings.bindBidirectional(databaseName.textProperty(), propertiesBean.getDatabaseNameProperty());
        Bindings.bindBidirectional(mysqlDatabasePort.textProperty(), propertiesBean.getMysqlDatabasePortProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(databaseUserName.textProperty(), propertiesBean.getDatabaseUserNameProperty());
        Bindings.bindBidirectional(databasePassword.textProperty(), propertiesBean.getDatabasePasswordProperty());
        Bindings.bindBidirectional(smtpServerUrl.textProperty(), propertiesBean.getSmtpServerUrlProperty());
        Bindings.bindBidirectional(smtpServerPort.textProperty(), propertiesBean.getSmtpServerPortProperty(), new NumberStringConverter());
    }

    /**
     * Validate if the input of the user is valid
     *
     * @return
     */
    private boolean validFields() {
        return !(propertiesBean.getUserName().isEmpty()
                || propertiesBean.getUserEmail().isEmpty()
                || propertiesBean.getUserPassword().isEmpty()
                || propertiesBean.getImapServerUrl().isEmpty()
                || propertiesBean.getImapServerPort() <= 0
                || propertiesBean.getMysqlDatabaseURL().isEmpty()
                || propertiesBean.getDatabaseName().isEmpty()
                || propertiesBean.getMysqlDatabasePort() <= 0
                || propertiesBean.getDatabaseUserName().isEmpty()
                || propertiesBean.getDatabasePassword().isEmpty()
                || propertiesBean.getSmtpServerUrl().isEmpty()
                || propertiesBean.getSmtpServerPort() <= 0
                || RFC2822AddressParser.STRICT.parseToEmailAddress(propertiesBean.getUserEmail()) == null);
    }

    /**
     * Creates an alert Message
     *
     * @param title the title
     * @param header the header
     * @param msg the content
     */
    private void alertMessage(String title, String header, String msg) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(msg);
        alert.showAndWait();
    }
}
