/**
 * Sample Skeleton for 'mailInterface.fxml' Controller Class
 */
package com.zhijiec99.JavaFx.fxcontroller;

import com.zhijiec99.JavaFx.application.MainApp;
import com.zhijiec99.JavaFx.fxbeans.FXFolderBean;
import com.zhijiec99.JavaFx.fxbeans.FXMailBean;
import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.JavaFx.manager.AttachmentManager;
import com.zhijiec99.JavaFx.manager.FXBeanManager;
import com.zhijiec99.JavaFx.manager.PropertiesManager;
import com.zhijiec99.beans.AttachmentBean;
import com.zhijiec99.beans.MailBean;
import com.zhijiec99.data.MessageType;
import com.zhijiec99.databasecontroller.FolderStorageModule;
import com.zhijiec99.databasecontroller.MailStorageModule;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.NumberStringConverter;
import jodd.mail.EmailAddress;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MailInterfaceController {

    private MessageType type;
    private PropertiesManager manager;
    private AttachmentManager attachmentManager;
    private FXPropertiesBean properties;
    private FolderStorageModule folderStorageModule;
    private MailStorageModule mailStorageModule;
    private List<FXFolderBean> folders;
    private FXBeanManager fxBeanManager;
    private List<FXMailBean> mail;
    private FXFolderBean currentlySelectedFolder;
    private FXMailBean currentlySelectedMailBean;
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="FolderTable"
    private TreeTableView<FXFolderBean> FolderTable; // Value injected by FXMLLoader

    @FXML // fx:id="FolderColumn"
    private TreeTableColumn<FXFolderBean, String> FolderColumn; // Value injected by FXMLLoader

    @FXML // fx:id="newMail"
    private Button newMail; // Value injected by FXMLLoader

    @FXML // fx:id="mailTable"
    private TableView<FXMailBean> mailTable; // Value injected by FXMLLoader

    @FXML // fx:id="fromColumn"
    private TableColumn<FXMailBean, EmailAddress> fromColumn; // Value injected by FXMLLoader

    @FXML // fx:id="subjectColumn"
    private TableColumn<FXMailBean, String> subjectColumn; // Value injected by FXMLLoader

    @FXML // fx:id="dateColumn"
    private TableColumn<FXMailBean, String> dateColumn; // Value injected by FXMLLoader

    @FXML // fx:id="replyMail"
    private Button replyMail; // Value injected by FXMLLoader

    @FXML // fx:id="forwardMail"
    private Button forwardMail; // Value injected by FXMLLoader

    @FXML // fx:id="deleteMail"
    private Button deleteMail; // Value injected by FXMLLoader

    @FXML // fx:id="messageBody"
    private WebView messageBody; // Value injected by FXMLLoader

    @FXML // fx:id="fromText"
    private Text fromText; // Value injected by FXMLLoader

    @FXML // fx:id="toText"
    private Text toText; // Value injected by FXMLLoader

    @FXML // fx:id="ccText"
    private Text ccText; // Value injected by FXMLLoader

    @FXML // fx:id="subjectText"
    private Text subjectText; // Value injected by FXMLLoader

    @FXML // fx:id="attachmentBox"
    private ListView attachmentBox; // Value injected by FXMLLoader

    @FXML // fx:id="addFolder"
    private Button addFolder; // Value injected by FXMLLoader

    @FXML // fx:id="deleteFolder"
    private Button deleteFolder; // Value injected by FXMLLoader

    public MailInterfaceController() {
        try {
            this.attachmentManager = new AttachmentManager();
            this.manager = new PropertiesManager();
            this.properties = manager.loadProperties();
            this.mailStorageModule = new MailStorageModule(properties);
            this.folderStorageModule = new FolderStorageModule(properties);
            this.fxBeanManager = new FXBeanManager();
            //insert mock data
            folders = new ArrayList<>();
            //mail = new ArrayList<>();

        } catch (SQLException | IOException ex) {
            alertMessage("Error", "An error occured during Initialization", ex.getMessage());
        }
    }

    /**
     * Called when a folder is added
     *
     * @param event
     */
    @FXML
    void onAddFolder(ActionEvent event) {
        String name = promptUser("Add Folder", "Add Folder", "Please enter the name of the folder:");
        if (name != null && !name.isEmpty()) {
            folders.add(new FXFolderBean(name, 0));
            try {
                folderStorageModule.createFolder(name);
            } catch (SQLException ex) {
                Logger.getLogger(MailInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
            }
            populateTable(folders);
        }
    }

    /**
     * Called when a folder is deleted
     *
     * @param event
     */
    @FXML
    void onDeleteFolder(ActionEvent event) {
        if (currentlySelectedFolder != null) {
            try {
                folderStorageModule.deleteFolder(currentlySelectedFolder.getFolderName());
                folders.remove(currentlySelectedFolder);
                currentlySelectedFolder = null;
                populateTable(folders);
            } catch (SQLException ex) {
                alertMessage("Error", "Could not delete folder", "The folder must be empty to be deleted \n" + ex.getMessage());
            }
        } else {
            alertMessage("Error", "No folder Selected", "Please select a folder to delete it.");
        }
    }

    /**
     * Called when delete mail button is clicked
     *
     * @param event
     */
    @FXML
    void onDeleteMail(ActionEvent event) {
        if (currentlySelectedMailBean != null) {
            try {
                mail.remove(currentlySelectedMailBean);
                mailStorageModule.deleteMail(fxBeanManager.convertFXMailBean(currentlySelectedMailBean));
                currentlySelectedMailBean = null;
                populateMailTable(mail);
                alertMessage("Success", "Successfully deleted mail", "Successfully deleted mail");
            } catch (SQLException ex) {
                alertMessage("Error", "Could not delete mail", ex.getMessage());
            }
        } else {
            alertMessage("Error", "There are no selected Mail", "Please select a mail to delete it.");
        }
    }

    /**
     * Called when a folder is selected
     *
     * @param event
     */
    @FXML
    void onFolderSelect(MouseEvent event) {

    }

    /**
     * Called when forward button is clicked
     *
     * @param event
     */
    @FXML
    void onForwardMail(ActionEvent event) {
        if (currentlySelectedMailBean != null) {
            type = MessageType.FORWARD;
            composeNewMail();
        } else {
            alertMessage("Error", "No Mail Selected", "Please select a mail to forward");
        }
    }

    /**
     * Called when a mail is selected
     *
     * @param event
     */
    @FXML
    void onMailSelect(MouseEvent event) {

    }

    /**
     * Called when the new mail button is clicked
     *
     * @param event
     */
    @FXML
    void onNewMail(ActionEvent event) {
        composeNewMail();
    }

    /**
     * Called when the reply button is clicked
     *
     * @param event
     */
    @FXML
    void onReplyMail(ActionEvent event) {
        if (currentlySelectedMailBean != null) {
            type = MessageType.REPLY;
            composeNewMail();
        } else {
            alertMessage("Error", "No Mail Selected", "Please select a mail to reply");
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert FolderTable != null : "fx:id=\"FolderTable\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert FolderColumn != null : "fx:id=\"FolderColumn\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert newMail != null : "fx:id=\"newMail\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert mailTable != null : "fx:id=\"mailTable\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert fromColumn != null : "fx:id=\"fromColumn\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert subjectColumn != null : "fx:id=\"subjectColumn\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert dateColumn != null : "fx:id=\"dateColumn\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert replyMail != null : "fx:id=\"replyMail\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert forwardMail != null : "fx:id=\"forwardMail\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert deleteMail != null : "fx:id=\"deleteMail\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert messageBody != null : "fx:id=\"messageBody\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert fromText != null : "fx:id=\"fromText\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert toText != null : "fx:id=\"toText\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert ccText != null : "fx:id=\"ccText\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert subjectText != null : "fx:id=\"subjectText\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert attachmentBox != null : "fx:id=\"attachmentBox\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert addFolder != null : "fx:id=\"addFolder\" was not injected: check your FXML file 'mailInterface.fxml'.";
        assert deleteFolder != null : "fx:id=\"deleteFolder\" was not injected: check your FXML file 'mailInterface.fxml'.";

        fromColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getFromProperty());
        subjectColumn.setCellValueFactory(cellData -> cellData.getValue().getSubjectProperty());
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().getReceivedTimeProperty().asString());

        try {
            Map<Integer, String> foldersMap = folderStorageModule.retrieveFolder();
            foldersMap.entrySet().forEach((folder) -> {
                folders.add(new FXFolderBean(folder.getValue(), folder.getKey()));
            });
            populateTable(folders);
        } catch (SQLException ex) {
            alertMessage("Error", "Failed retrieving database", "Are you sure the database is configured properly?\n" + ex.getMessage());
        }

        //EVENT listener for folders
        FolderTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue,
                        newValue) -> {
                    TreeItem<FXFolderBean> selectedItem = (TreeItem<FXFolderBean>) newValue;
                    if (selectedItem.getValue().getFolderId() != -1) {
                        try {
                            currentlySelectedFolder = selectedItem.getValue();
                            List<MailBean> mailFromFolder = mailStorageModule.retrieveMailByFolder(selectedItem.getValue().getFolderName());
                            mail = new ArrayList<>();
                            for (MailBean bean : mailFromFolder) {
                                FXBeanManager fbm = new FXBeanManager();
                                mail.add(fbm.convertMailBean(bean));
                            }
                            mailTable.getItems().clear();
                            currentlySelectedMailBean = null;
                            populateMailTable(mail);
                        } catch (SQLException ex) {
                            alertMessage("Error", "SQL Exception", ex.getMessage());
                        }
                    }

                });
        //Event listener for mail
        mailTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue instanceof FXMailBean) {
                FXMailBean bean = newValue;
                displayMail(bean);
                currentlySelectedMailBean = newValue;
            }
        });

    }

    /**
     * Launches a new window to compose a mail
     */
    private void composeNewMail() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/messageComposition.fxml"));
            Parent root;
            root = loader.load();
            Scene scene = new Scene(root);
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Compose Mail");
            primaryStage.setScene(scene);
            MessageCompositionController mcc = loader.getController();
            if (currentlySelectedMailBean != null) {
                if (type == MessageType.FORWARD) {
                    mcc.setForward(currentlySelectedMailBean);
                } else if (type == MessageType.REPLY) {
                    mcc.setReply(currentlySelectedMailBean);
                }
            }
            
            primaryStage.show();
        } catch (IOException ex) {
            Logger.getLogger(MailInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method populates the table from a list of folders
     *
     * @param folders
     */
    private void populateTable(List<FXFolderBean> folders) {
        TreeItem<FXFolderBean> root = new TreeItem<>(new FXFolderBean("Folders", -1));
        for (FXFolderBean folder : folders) {
            root.getChildren().add(new TreeItem<>(folder));
        }
        FolderColumn.setCellValueFactory(cellData -> cellData.getValue().getValue().getFolderNameProperty());

        FolderTable.setRoot(root);
        FolderTable.getRoot().setExpanded(true);
    }

    private void populateMailTable(List<FXMailBean> mail) {
        ObservableList<FXMailBean> observableMail = FXCollections.observableArrayList(mail);
        mailTable.setItems(observableMail);
    }

    /**
     * Displays a single mail
     *
     * @param mail
     */
    private void displayMail(FXMailBean mail) {
        fromText.textProperty().bind(mail.getFromProperty().asString());
        toText.textProperty().bind(mail.getToProperty().asString());
        ccText.textProperty().bind(mail.getCcProperty().asString());
        subjectText.textProperty().bind(mail.getSubjectProperty());
        WebEngine engine = messageBody.getEngine();
        for (AttachmentBean att : mail.getEmbeddedAttachments()) {
            try {
                FileOutputStream fos = new FileOutputStream(new File("C:/Temp/" + att.getName()));
                fos.write(att.getContent());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MailInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MailInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        String html = mail.getHtmlMessage();
        html = html.replaceAll("cid:", "file:///C:/Temp/");
        engine.loadContent("<html>" + (mail.getHtmlMessage().isEmpty() ? mail.getTextMessage() : html) + "</html>");
        displayAttachments(mail);
    }

    /**
     * Displays all the attachments of a selected mail
     *
     * @param mail
     */
    private void displayAttachments(FXMailBean mail) {
        attachmentBox.getItems().clear();
        mail.getAttachments().forEach(e -> {
            Button b = new Button(e.getName());
            attachmentBox.getItems().add(b);
            b.setOnAction(event -> {
                try {
                    attachmentManager.downloadAttachment(e, attachmentBox.getScene());
                } catch (IOException ex) {
                    alertMessage("Error", "Error saving file", ex.getMessage());
                    Logger.getLogger(MailInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        });
        mail.getEmbeddedAttachments().forEach(e -> {
            Button b = new Button(e.getName());
            attachmentBox.getItems().add(b);
            b.setOnAction(event -> {
                try {
                    attachmentManager.downloadAttachment(e, attachmentBox.getScene());
                } catch (IOException ex) {
                    alertMessage("Error", "Error saving file", ex.getMessage());
                    Logger.getLogger(MailInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        });
    }

    /**
     * Moves the mail to a specific folder
     *
     * @param event
     */
    @FXML
    void onMoveMail(ActionEvent event) {
        if (currentlySelectedMailBean != null) {
            String name = promptUser("Move Email", "Change Folder", "Please enter the name of the folder:");
            try {
                if (name != null && !name.isEmpty() && folderStorageModule.retrieveFolder().containsValue(name)) {
                    try {
                        currentlySelectedMailBean.setFolderName(name);
                        mailStorageModule.deleteMail(fxBeanManager.convertFXMailBean(currentlySelectedMailBean));
                        mailStorageModule.insertMail(fxBeanManager.convertFXMailBean(currentlySelectedMailBean));
                        mail.remove(currentlySelectedMailBean);
                        currentlySelectedMailBean = null;
                    } catch (SQLException ex) {
                        Logger.getLogger(MailInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    populateMailTable(mail);
                } else {
                    alertMessage("Failed", "You entered nothing or folder doesn't exist", "Please enter a valid folder name");
                }
            } catch (SQLException ex) {
                alertMessage("Error", "Something went wrong", "Error while ");
            }
        } else {
            alertMessage("Error", "No mail selected", "Please select a mail to move it");
        }
    }

    /**
     * Happens when you press configure
     *
     * @param event
     */
    @FXML
    void onReconfigure(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/configComposition.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Create configuration");
            primaryStage.setScene(scene);
            primaryStage.show();
            ((Stage) ((javafx.scene.Node) event.getSource()).getScene().getWindow()).close();
        } catch (IOException ioe) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ioe);
        }
    }

    /**
     * Prompts the user for input
     *
     * @param title the title
     * @param header the header
     * @param msg the content
     * @return string they input
     */
    private String promptUser(String title, String header, String message) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        dialog.setHeaderText(header);
        dialog.setContentText(message);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            return result.get();
        }
        return null;
    }

    /**
     * Creates an alert Message
     *
     * @param title the title
     * @param header the header
     * @param msg the content
     */
    private void alertMessage(String title, String header, String msg) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(msg);
        alert.showAndWait();
    }
}
