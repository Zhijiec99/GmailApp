/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.JavaFx.manager;

import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * This class is responsible to manage properties.
 *
 * @author Zhijie Cao
 */
public class PropertiesManager {

    private final Properties properties;
    private final String PATH = "./src/test/resources/Properties";

    public PropertiesManager() {
        properties = new Properties();
        }

    /**
     * Save the properties of a bean
     * @param bean the bean to save
     * @throws IOException 
     */
    public void saveProperties(FXPropertiesBean bean) throws IOException {
        File file = new File(PATH);
        properties.put("userName", bean.getUserName());
        properties.put("userEmail", bean.getUserEmail());
        properties.put("userPassword", bean.getUserPassword());
        properties.put("ImapServerUrl", bean.getImapServerUrl());
        properties.put("SmtpServerUrl", bean.getSmtpServerUrl());
        properties.put("ImapServerPort", "" + bean.getImapServerPort());
        properties.put("SmtpServerPort", "" + bean.getSmtpServerPort());
        properties.put("mysqlDatabaseURL", bean.getMysqlDatabaseURL());
        properties.put("mysqlDatabasePort", "" + bean.getMysqlDatabasePort() );
        properties.put("databaseName", bean.getDatabaseName());
        properties.put("databaseUserName", bean.getDatabaseUserName());
        properties.put("databasePassword", bean.getDatabasePassword());
        FileOutputStream stream = new FileOutputStream(file);
        properties.store(stream, "Properties File");
    }

    /**
     * Loads the properties from the default PATH
     * @return FXPropertiesBean
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public FXPropertiesBean loadProperties() throws FileNotFoundException, IOException {
        File file = new File(PATH);
        InputStream stream = new FileInputStream(file);
        properties.load(stream);
        return new FXPropertiesBean(properties.getProperty("userName"),
                properties.getProperty("userEmail"),
                properties.getProperty("userPassword"),
                properties.getProperty("ImapServerUrl"),
                properties.getProperty("SmtpServerUrl"),
                Integer.parseInt(properties.getProperty("ImapServerPort", "993")),
                Integer.parseInt(properties.getProperty("SmtpServerPort", "465")),
                properties.getProperty("mysqlDatabaseURL"),
                Integer.parseInt(properties.getProperty("mysqlDatabasePort", "3306")),
                properties.getProperty("databaseName"),
                properties.getProperty("databaseUserName"),
                properties.getProperty("databasePassword"));
    }
}
