/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.JavaFx.manager;

import com.zhijiec99.JavaFx.fxbeans.FXMailBean;
import com.zhijiec99.beans.MailBean;
import jodd.mail.EmailAddress;

/**
 *
 * @author Zhijie Cao
 */
public class FXBeanManager {

    public FXBeanManager() {

    }

    public MailBean convertFXMailBean(FXMailBean bean) {
        return new MailBean(
                bean.getFrom(),
                bean.getTo().toArray(new EmailAddress[0]),
                bean.getCc().toArray(new EmailAddress[0]),
                bean.getBcc().toArray(new EmailAddress[0]),
                bean.getSubject(),
                bean.getTextMessage(),
                bean.getHtmlMessage(), 
                bean.getAttachments(),
                bean.getEmbeddedAttachments(),
                bean.getMessageType(),
                bean.getSentTime(),
                bean.getReceivedTime(),
                bean.getFolderName(),
                bean.getPriority(),
                bean.getEmailId());
    }
    
    public FXMailBean convertMailBean(MailBean bean){
        return new FXMailBean(
                bean.getFrom(),
                bean.getTo(),
                bean.getCc(),
                bean.getBcc(),
                bean.getSubject(),
                bean.getTextMessage(),
                bean.getHtmlMessage(),
                bean.getAttachments(),
                bean.getEmbeddedAttachments(), 
                bean.getMessageType(),
                bean.getSentTime(),
                bean.getReceivedTime(),
                bean.getFolderName(),
                bean.getPriority(),
                bean.getEmailId()
        );
    }
    
}
