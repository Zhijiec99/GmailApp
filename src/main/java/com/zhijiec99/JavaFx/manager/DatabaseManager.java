/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.JavaFx.manager;

import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.databasecontroller.MailStorageModule;
import com.zhijiec99.mailcontroller.IMAPModule;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Zhijie Cao
 */
public class DatabaseManager {
    
    public DatabaseManager(){
        
    }
    
    /**
     * Seeds the database with the new emails.
     * @param config
     * @throws SQLException
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void seedDatabase(FXPropertiesBean config) throws SQLException, FileNotFoundException, IOException {
//        final String sqlFile = loadString("./src/main/resources/database/JAG_Create.sql");
//        try (Connection connection = DriverManager.getConnection(
//                config.generateDatabaseURL(),
//                config.getDatabaseUserName(),
//                config.getDatabasePassword());) {
//            connection.prepareStatement("USE " + config.getDatabaseName()).execute();
//            for (String statement : splitStatements(new StringReader(sqlFile), ";")) {
//                connection.prepareStatement(statement).execute();
//            }
//        }
        IMAPModule imap = new IMAPModule(config);
        MailStorageModule msm = new MailStorageModule(config);
        msm.insertMail(imap.receiveMail());
    }
//
//    private String loadString(String path) throws FileNotFoundException, IOException {
//        try (InputStream inputStream = new FileInputStream(new File(path)); ) {
//            Scanner scanner = new Scanner(inputStream);
//            return scanner.useDelimiter("\\A").next();
//        }
//    }
//    
//    /**
//     * Splits the statements
//     * @param reader
//     * @param stmtDelimiter
//     * @return a list of statements
//     * @Author Ken
//     */
//    private List<String> splitStatements(Reader reader, String stmtDelimiter) {
//        BufferedReader buffReader = new BufferedReader(reader);
//        StringBuilder sqlStmnt = new StringBuilder();
//        List<String> statements = new LinkedList<>();
//        try {
//            String line;
//            while ((line = buffReader.readLine()) != null) {
//                line = line.trim();
//                if (line.isEmpty() || isComment(line)) {
//                    continue;
//                }
//                sqlStmnt.append(line);
//                if (line.endsWith(stmtDelimiter)) {
//                    statements.add(sqlStmnt.toString());
//                    sqlStmnt.setLength(0);
//                }
//            }
//            return statements;
//        } catch (IOException e) {
//            throw new RuntimeException("Failed parsing sql", e);
//        }
//    }
//
//    /**
//     * check if a line is a comment
//     * @param line
//     * @return if its a comment or not
//     * @Author Ken
//     */
//    private boolean isComment(final String line) {
//        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
//    }
}
