/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.JavaFx.manager;

import com.zhijiec99.beans.AttachmentBean;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javafx.scene.Scene;
import javafx.stage.FileChooser;

/**
 *
 * @author Zhijie Cao
 */
public class AttachmentManager {
    
    /**
     * Displays a window to save an attachment
     * @param e
     * @param parent
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void downloadAttachment(AttachmentBean e, Scene parent) throws FileNotFoundException, IOException {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Any", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("png", "*.png"));
        File f = fc.showSaveDialog(parent.getWindow());
        if (f != null) {
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(e.getContent());
        }
    }
    
    /**
     * Displays a window to load an attachment
     * @param parent
     * @return the attachmentBean
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public AttachmentBean loadAttachment(Scene parent) throws FileNotFoundException, IOException{
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Any", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("png", "*.png"));
        File f = fc.showOpenDialog(parent.getWindow());
        if (f != null) {
            return new AttachmentBean(f.getName(), Files.readAllBytes(Paths.get(f.getPath())));
        } else {
            return null;
        }
    }
}
