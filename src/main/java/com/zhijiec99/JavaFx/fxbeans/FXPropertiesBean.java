/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.JavaFx.fxbeans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Zhijie Cao
 */
public class FXPropertiesBean {

    private final StringProperty userName;
    private final StringProperty userEmail;
    private final StringProperty userPassword;

    private final StringProperty ImapServerUrl;
    private final StringProperty SmtpServerUrl;
    private final IntegerProperty ImapServerPort;
    private final IntegerProperty SmtpServerPort;
    //TODO: database information;
    private final StringProperty mysqlDatabaseURL;
    private final StringProperty databaseName;
    private final IntegerProperty mysqlDatabasePort;
    private final StringProperty databaseUserName;
    private final StringProperty databasePassword;

    public FXPropertiesBean() {
        this("", "", "", "imap.gmail.com", "smtp.gmail.com", 993, 465, "localhost", 3306, "", "", "");
    }

    public FXPropertiesBean(
            String userName,
            String userEmail,
            String userPassword,
            String ImapServerUrl,
            String SmtpServerUrl,
            int ImapServerPort,
            int SmtpServerPort,
            String mysqlDatabaseUrl,
            int mysqlDatabasePort,
            String databaseName,
            String databaseUserName,
            String databasePassword) {
        this.userName = new SimpleStringProperty(userName);
        this.userEmail = new SimpleStringProperty(userEmail);
        this.userPassword = new SimpleStringProperty(userPassword);
        this.ImapServerUrl = new SimpleStringProperty(ImapServerUrl);
        this.SmtpServerUrl = new SimpleStringProperty(SmtpServerUrl);
        this.ImapServerPort = new SimpleIntegerProperty(ImapServerPort);
        this.SmtpServerPort = new SimpleIntegerProperty(SmtpServerPort);
        this.mysqlDatabaseURL = new SimpleStringProperty(mysqlDatabaseUrl);
        this.databaseName = new SimpleStringProperty(databaseName);
        this.mysqlDatabasePort = new SimpleIntegerProperty(mysqlDatabasePort);
        this.databaseUserName = new SimpleStringProperty(databaseUserName);
        this.databasePassword = new SimpleStringProperty(databasePassword);
    }

    public String getUserName() {
        return userName.get();
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public String getUserEmail() {
        return userEmail.get();
    }

    public void setUserEmail(String userEmail) {
        this.userEmail.set(userEmail);
    }

    public String getUserPassword() {
        return userPassword.get();
    }

    public void setUserPassword(String userPassword) {
        this.userPassword.set(userPassword);
    }

    public String getImapServerUrl() {
        return ImapServerUrl.get();
    }

    public void setImapServerUrl(String ImapServerUrl) {
        this.ImapServerUrl.set(ImapServerUrl);
    }

    public String getSmtpServerUrl() {
        return SmtpServerUrl.get();
    }

    public void setSmtpServerUrl(String SmtpServerUrl) {
        this.SmtpServerUrl.set(SmtpServerUrl);
    }

    public int getImapServerPort() {
        return ImapServerPort.get();
    }

    public void setImapServerPort(int ImapServerPort) {
        this.ImapServerPort.set(ImapServerPort);
    }

    public int getSmtpServerPort() {
        return SmtpServerPort.get();
    }

    public void setSmtpServerPort(int SmtpServerPort) {
        this.SmtpServerPort.set(SmtpServerPort);
    }

    /**
     * This method is used to generate the url, so the user only needs to enter the domain of the database.
     * @return 
     */
    public String generateDatabaseURL() {
        return "jdbc:mysql://" + this.mysqlDatabaseURL.get() + ":" + getMysqlDatabasePort() + "/"+ this.getDatabaseName() + "?zeroDateTimeBehavior=CONVERT_TO_NULL";
    }
    
    public String getMysqlDatabaseURL(){
        return this.mysqlDatabaseURL.get();
    }
    

    public void setMysqlDatabaseURL(String mysqlDatabaseURL) {
        this.mysqlDatabaseURL.set(mysqlDatabaseURL);
    }

    public String getDatabaseName() {
        return databaseName.get();
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName.set(databaseName);
    }

    public int getMysqlDatabasePort() {
        return mysqlDatabasePort.get();
    }

    public void setMysqlDatabasePort(int mysqlDatabasePort) {
        this.mysqlDatabasePort.set(mysqlDatabasePort);
    }

    public String getDatabaseUserName() {
        return databaseUserName.get();
    }

    public void setDatabaseUserName(String databaseUserName) {
        this.databaseUserName.set(databaseUserName);
    }

    public String getDatabasePassword() {
        return databasePassword.get();
    }

    public void setDatabasePassword(String databasePassword) {
        this.databasePassword.set(databasePassword);
    }

    public StringProperty getUserNameProperty() {
        return userName;
    }

    public StringProperty getUserEmailProperty() {
        return userEmail;
    }

    public StringProperty getUserPasswordProperty() {
        return userPassword;
    }

    public StringProperty getImapServerUrlProperty() {
        return ImapServerUrl;
    }

    public StringProperty getSmtpServerUrlProperty() {
        return SmtpServerUrl;
    }

    public IntegerProperty getImapServerPortProperty() {
        return ImapServerPort;
    }

    public IntegerProperty getSmtpServerPortProperty() {
        return SmtpServerPort;
    }

    public StringProperty getMysqlDatabaseURLProperty() {
        return mysqlDatabaseURL;
    }

    public StringProperty getDatabaseNameProperty() {
        return databaseName;
    }

    public IntegerProperty getMysqlDatabasePortProperty() {
        return mysqlDatabasePort;
    }

    public StringProperty getDatabaseUserNameProperty() {
        return databaseUserName;
    }

    public StringProperty getDatabasePasswordProperty() {
        return databasePassword;
    }

}
