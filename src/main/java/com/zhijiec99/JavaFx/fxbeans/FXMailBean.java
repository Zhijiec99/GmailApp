/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.JavaFx.fxbeans;

import com.zhijiec99.beans.AttachmentBean;
import com.zhijiec99.beans.MailBean;
import com.zhijiec99.data.EmailPriority;
import com.zhijiec99.data.MessageType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import jodd.mail.EmailAddress;

/**
 *
 * @author Zhijie Cao
 */
public class FXMailBean implements Serializable {

    private final ObjectProperty<EmailAddress> from;
    private final ListProperty<EmailAddress> to;
    private final ListProperty<EmailAddress> cc;
    private final ListProperty<EmailAddress> bcc;
    private final StringProperty subject;
    private final StringProperty textMessage;
    private final StringProperty htmlMessage;
    private final ListProperty<AttachmentBean> attachments;
    private final ListProperty<AttachmentBean> embeddedAttachments;
    private final ObjectProperty<MessageType> messageType;
    private final ObjectProperty<Date> sentTime;
    private final ObjectProperty<Date> receivedTime;
    private final StringProperty folderName;
    private final ObjectProperty<EmailPriority> priority;
    private final IntegerProperty emailId;

    public FXMailBean() {
        this(null, new EmailAddress[0], new EmailAddress[0], new EmailAddress[0], "", "", "", new ArrayList<AttachmentBean>(), new ArrayList<AttachmentBean>(), null, null, null, "", null, 0);
    }

    public FXMailBean(int id) {
        this(null, new EmailAddress[0], new EmailAddress[0], new EmailAddress[0], "", "", "", new ArrayList<AttachmentBean>(), new ArrayList<AttachmentBean>(), null, null, null, "", null, id);
    }

    public FXMailBean(
            EmailAddress from,
            EmailAddress[] to,
            EmailAddress[] cc,
            EmailAddress[] bcc,
            String subject,
            String textMessage,
            String htmlMessage,
            List<AttachmentBean> attachments,
            List<AttachmentBean> embeddedAttachments,
            MessageType messageType,
            Date sentTime,
            Date receivedTime,
            String folderName,
            EmailPriority priority,
            int id) {
        this.from = new SimpleObjectProperty<>(from);
        this.to = new SimpleListProperty(FXCollections.observableArrayList(to));
        this.cc = new SimpleListProperty(FXCollections.observableArrayList(cc));
        this.bcc = new SimpleListProperty(FXCollections.observableArrayList(bcc));
        this.subject = new SimpleStringProperty(subject);
        this.textMessage = new SimpleStringProperty(textMessage);
        this.htmlMessage = new SimpleStringProperty(htmlMessage);
        this.attachments = new SimpleListProperty(FXCollections.observableArrayList(attachments));
        this.embeddedAttachments = new SimpleListProperty(FXCollections.observableArrayList(embeddedAttachments));
        this.messageType = new SimpleObjectProperty(messageType);
        this.sentTime = new SimpleObjectProperty(sentTime);
        this.receivedTime = new SimpleObjectProperty(receivedTime);
        this.folderName = new SimpleStringProperty(folderName);
        this.priority = new SimpleObjectProperty(priority);
        this.emailId = new SimpleIntegerProperty(id);
    }

    public ObjectProperty<EmailAddress> getFromProperty() {
        return from;
    }

    public ListProperty<EmailAddress> getToProperty() {
        return to;
    }

    public ListProperty<EmailAddress> getCcProperty() {
        return cc;
    }

    public ListProperty<EmailAddress> getBccProperty() {
        return bcc;
    }

    public StringProperty getSubjectProperty() {
        return subject;
    }

    public StringProperty getTextMessageProperty() {
        return textMessage;
    }

    public StringProperty getHtmlMessageProperty() {
        return htmlMessage;
    }

    public ListProperty<AttachmentBean> getAttachmentsProperty() {
        return attachments;
    }

    public ListProperty<AttachmentBean> getEmbeddedAttachmentsProperty() {
        return embeddedAttachments;
    }

    public ObjectProperty<MessageType> getMessageTypeProperty() {
        return messageType;
    }

    public ObjectProperty<Date> getSentTimeProperty() {
        return sentTime;
    }

    public ObjectProperty<Date> getReceivedTimeProperty() {
        return receivedTime;
    }

    public StringProperty getFolderNameProperty() {
        return folderName;
    }

    public ObjectProperty<EmailPriority> getPriorityProperty() {
        return priority;
    }

    public IntegerProperty getEmailIdProperty() {
        return emailId;
    }

    /**
     * Get the emailId
     *
     * @return
     */
    public int getEmailId() {
        return emailId.get();
    }

    /**
     * Get the FROM
     *
     * @return
     */
    public EmailAddress getFrom() {
        return from.get();
    }

    /**
     * Get the TO
     *
     * @return
     */
    public List<EmailAddress> getTo() {
        return to.get();
    }

    /**
     * Get the CC
     *
     * @return
     */
    public List<EmailAddress> getCc() {
        return cc.get();
    }

    /**
     * Get the BCC
     *
     * @return
     */
    public List<EmailAddress> getBcc() {
        return bcc.get();
    }

    /**
     * Get the subject
     *
     * @return
     */
    public String getSubject() {
        return subject.get();
    }

    /**
     * Get the MESSAGE
     *
     * @return
     */
    public String getTextMessage() {
        return textMessage.get();
    }

    /**
     * Get the HTMLMESSAGE
     *
     * @return
     */
    public String getHtmlMessage() {
        return htmlMessage.get();
    }

    /**
     * Get the attachments
     *
     * @return
     */
    public List<AttachmentBean> getAttachments() {
        return attachments.get();
    }

    /**
     * Get the embeddedAttachments
     *
     * @return
     */
    public List<AttachmentBean> getEmbeddedAttachments() {
        return embeddedAttachments.get();
    }

    /**
     * Get the messageType
     *
     * @return
     */
    public MessageType getMessageType() {
        return messageType.get();
    }

    /**
     * Get the sentTime
     *
     * @return
     */
    public Date getSentTime() {
        return sentTime.get();
    }

    /**
     * Get the receiveTime
     *
     * @return
     */
    public Date getReceivedTime() {
        return receivedTime.get();
    }

    /**
     * Get the folderName
     *
     * @return
     */
    public String getFolderName() {
        return folderName.get();
    }

    /**
     * Get the Priority
     *
     * @return
     */
    public EmailPriority getPriority() {
        return priority.get();
    }

    public void setFrom(EmailAddress from) {
        this.from.set(from);
    }

    public void setTo(EmailAddress[] to) {
        this.to.set(FXCollections.observableArrayList(to));
    }

    public void setCc(EmailAddress[] cc) {
        this.cc.set(FXCollections.observableArrayList(cc));
    }

    public void setBcc(EmailAddress[] bcc) {
        this.bcc.set(FXCollections.observableArrayList(bcc));
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }

    public void setTextMessage(String textMessage) {
        this.textMessage.set(textMessage);
    }

    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage.set(htmlMessage);
    }

    public void setAttachments(List<AttachmentBean> attachments) {
        this.attachments.set(FXCollections.observableArrayList(attachments));
    }

    public void setEmbeddedAttachments(List<AttachmentBean> embeddedAttachments) {
        this.embeddedAttachments.set(FXCollections.observableArrayList(embeddedAttachments));
    }

    public void setMessageType(MessageType messageType) {
        this.messageType.set(messageType);
    }

    public void setSentTime(Date sentTime) {
        this.sentTime.set(sentTime);
    }

    public void setReceivedTime(Date receivedTime) {
        this.receivedTime.set(receivedTime);
    }

    public void setFolderName(String folderName) {
        this.folderName.set(folderName);
    }

    public void setPriority(EmailPriority priority) {
        this.priority.set(priority);
    }

    public void setEmailId(int emailId) {
        this.emailId.set(emailId);
    }
}
