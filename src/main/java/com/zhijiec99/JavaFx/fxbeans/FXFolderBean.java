/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.JavaFx.fxbeans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Zhijie Cao
 */
public class FXFolderBean {
    private StringProperty folderName;
    private IntegerProperty folderId;
    
    public FXFolderBean(String folderName, int folderId){
        this.folderName = new SimpleStringProperty(folderName);
        this.folderId = new SimpleIntegerProperty(folderId);
    }
    public StringProperty getFolderNameProperty(){
        return folderName;
    }
    
    public IntegerProperty getFolderIdProperty(){
        return folderId;
    }
    
    public String getFolderName(){
        return folderName.get();
    }
    
    public int getFolderId(){
        return folderId.get();
    }
    
    public void setFolderName(String folderName){
        this.folderName.set(folderName);
    }
    
    public void setId(int id){
        this.folderId.set(id);
    }
}
