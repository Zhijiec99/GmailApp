/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.mailcomparator;

import com.zhijiec99.beans.MailBean;
import java.util.Comparator;

/**
 * Comparator to compare by sent/receive date.
 * @author Zhijie Cao
 */

public class MailBeanSentDateComparator implements Comparator<MailBean> {
    
    /**
     * Uses the Date object to compare the two
     * @param o1, first mailBean object
     * @param o2, second mailBean object
     * @return -1, 0, or 1, for smaller, equal and greater.
     */
    @Override
    public int compare(MailBean o1, MailBean o2) {
        return o1.getSentTime().compareTo(o2.getSentTime());
    }
}
