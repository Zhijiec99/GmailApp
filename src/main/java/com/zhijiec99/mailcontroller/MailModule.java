/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.mailcontroller;

import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.configurationcontroller.GMailConfig;
import java.util.logging.Logger;
import jodd.mail.RFC2822AddressParser;

/**
 * Parent class for mail Modules
 * @author Zhijie Cao
 */
public abstract class MailModule {
    private FXPropertiesBean gmailConfig;
    private Logger LOG;
    
    /**
     * Constructor for a mailModule
     * @param gmailConfig
     */
    public MailModule(FXPropertiesBean gmailConfig){
         if (gmailConfig == null) {
            throw new IllegalArgumentException("GMailConfig cannot be null");
        } 
        this.gmailConfig = gmailConfig;
        LOG = Logger.getLogger(MailModule.class.getName());
    }
    
    /**
     * Use RFC2822AddressParser to validate email address.
     *
     * @param address
     * @return true if valid email, false if not.
     * @author Ken
     */
    protected final boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
    
    protected FXPropertiesBean getGMailConfig(){
        return this.gmailConfig;
    }
    
    /**
     * Logs a message using the Logger`
     * @param message 
     */
    protected void log(String message) {
        if (LOG != null) {
            LOG.info(message);
        }
    }
}
