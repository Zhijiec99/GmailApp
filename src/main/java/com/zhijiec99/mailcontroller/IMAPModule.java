/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.mailcontroller;

import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.data.EmailPriority;
import com.zhijiec99.data.MessageType;
import com.zhijiec99.beans.MailBean;
import com.zhijiec99.configurationcontroller.GMailConfig;
import com.zhijiec99.beans.AttachmentBean;
import java.util.ArrayList;
import java.util.List;
import javax.activation.DataSource;
import javax.mail.Flags;
import jodd.mail.EmailFilter;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.net.MimeTypes;

/**
 * IMAPModule is for receiving email.
 * @author Zhijie Cao
 */
public class IMAPModule extends MailModule {
    /**
     * The Constructor
     * @param gmailConfig 
     */
    public IMAPModule(FXPropertiesBean gmailConfig) {
        super(gmailConfig);
    }

    /**
     * @return List the list of messages received
     * @author Ken, Zhi
     * @throws IllegalArgumentException, when the email contains incompatible
     * mime type, the email address is wrong.
     */
    public List<MailBean> receiveMail() {
        if (!checkEmail(getGMailConfig().getUserEmail())) {
            throw new IllegalArgumentException("Email is invalid");
        }

        List<MailBean> mailBeans = new ArrayList<>();
        ImapServer imapServer = MailServer.create()
                .ssl(true)
                .host(getGMailConfig().getImapServerUrl())
                .port(getGMailConfig().getImapServerPort())
                .auth(getGMailConfig().getUserEmail(), getGMailConfig().getUserPassword())
                .debugMode(true)
                .buildImapMailServer();
        try (ReceiveMailSession session = imapServer.createSession()) {
            session.open();
            log("Successfully logged in as: " + getGMailConfig().getUserEmail());
            ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
            if (emails != null) {

                log("-------------------GETTING MESSAGES------------------");
                log("You have: " + emails.length + "emails");
                log("\n");
                //FOR every email that isn't seen;

                for (ReceivedEmail email : emails) {
                    log("Processing email: " + email.subject());
                    List<EmailMessage> messages = email.messages();
                    String htmlMessage = "";
                    String textMessage = "";
                    //appends the plainText and html content to the tempMailBean
                    for (EmailMessage msg : messages) {

                        if (msg.getMimeType().equalsIgnoreCase(MimeTypes.MIME_TEXT_HTML)) {
                            htmlMessage
                                    += ""
                                    + msg.getContent();
                        } else if (msg.getMimeType().equalsIgnoreCase(MimeTypes.MIME_TEXT_PLAIN)) {
                            textMessage
                                    += ""
                                    + msg.getContent();
                        }
                    }
                    log("   # of message in Email: " + messages.size());
                    List<AttachmentBean> tempEmbeddedAttachments = new ArrayList<>();
                    List<AttachmentBean> tempAttachments = new ArrayList<>();
                    //process attachments
                    List<EmailAttachment<? extends DataSource>> attachments = email.attachments();
                    if (attachments != null) {
                        for (EmailAttachment attachment : attachments) {
                            log("   Processing attachment: " + attachment.getName());
                            if (attachment.isEmbedded()) {
                                tempEmbeddedAttachments.add(
                                        new AttachmentBean(attachment.getName(), attachment.toByteArray())
                                );
                            } else {
                                tempAttachments.add(
                                        new AttachmentBean(attachment.getName(), attachment.toByteArray())
                                );
                            }
                        }
//                        attachments.stream().forEachOrdered((attachment) -> {
//                            attachment.writeToFile(
//                                    new File("c:\\temp", attachment.getName()));
//                        });
                    }

                    MailBean tempMailBean = new MailBean(
                            email.from(),
                            email.to(),
                            email.cc(),
                            null,
                            email.subject(),
                            textMessage,
                            htmlMessage,
                            tempAttachments,
                            tempEmbeddedAttachments,
                            MessageType.NEW,
                            email.sentDate(),
                            email.receivedDate(),
                            "INBOX",
                            EmailPriority.getPriority(email.priority())
                    );
                    mailBeans.add(tempMailBean);
                }
            }
        }
        return mailBeans;
    }

}
