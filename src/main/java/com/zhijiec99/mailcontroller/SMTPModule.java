/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.mailcontroller;

import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.configurationcontroller.GMailConfig;
import com.zhijiec99.beans.AttachmentBean;
import com.zhijiec99.beans.MailBean;
import com.zhijiec99.data.EmailPriority;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.MailServer;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

/**
 * This module is used for sending emails. It extends an MailModule.
 *
 * @author Zhijie Cao
 */
public class SMTPModule extends MailModule {

    /**
     * Constructor for SMTPModule
     * @param gmailConfig
     */
    public SMTPModule(FXPropertiesBean gmailConfig) {
        super(gmailConfig);
    }

    /**
     * Sends a specific MailBean
     * @param mailBean
     */
    public void sendEmail(MailBean mailBean) {
        if (mailBean == null) {
            throw new IllegalArgumentException("mailBean cannot be null");
        }
        if (mailBean.getTo() == null) {
            throw new IllegalArgumentException("To cannot be null");
        }
        if (!mailBean.getFrom().getEmail().equals(getGMailConfig().getUserEmail())) {
            throw new IllegalArgumentException("You cannot send an email that doesn't belong to you.");
        }

        // Create am SMTP server object
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(getGMailConfig().getSmtpServerUrl())
                .auth(getGMailConfig().getUserEmail(),
                        getGMailConfig().getUserPassword())
                .port(getGMailConfig().getSmtpServerPort())
                .debugMode(true)
                .buildSmtpMailServer();

        // Using the fluent style requires EmailMessage
        Email email = Email.create()
                .from(getGMailConfig().getUserEmail())
                .to(mailBean.getTo())
                .cc(mailBean.getCc())
                .bcc(mailBean.getBcc())
                .subject(mailBean.getSubject())
                .sentDate(null)
                .priority((mailBean.getPriority() == null ? EmailPriority.PRIORITY_NORMAL.getValue() : mailBean.getPriority().getValue()));
        if (mailBean.getTextMessage() == null) {
            email.textMessage("");
        } else {
            email.textMessage(mailBean.getTextMessage());
        }
        
        if (mailBean.getHtmlMessage() == null) {
            email.htmlMessage("");
        } else {
            email.htmlMessage(mailBean.getHtmlMessage());
        }
        
        if (mailBean.getAttachments() != null) {
            for (AttachmentBean attachmentBean : mailBean.getAttachments()) {
                if (attachmentBean != null) {
                    email.attachment(EmailAttachment.with()
                            .content(attachmentBean.getContent())
                            .name(attachmentBean.getName()));
                }
            }
        }
        
        if (mailBean.getEmbeddedAttachments() != null) {
            for (AttachmentBean attachmentBean : mailBean.getEmbeddedAttachments()) {
                if (attachmentBean != null) {
                    email.embeddedAttachment(EmailAttachment.with()
                            .content(attachmentBean.getContent())
                            .name(attachmentBean.getName())
                    );
                }
            }
        }
        // Like a file we open the session, send the message and close the
        // session
        try (SendMailSession session = smtpServer.createSession()) {
            // Like a file we open the session, send the message and close the
            // session
            session.open();
            session.sendMail(email);
            log("Email sent");
        }

    }
}
