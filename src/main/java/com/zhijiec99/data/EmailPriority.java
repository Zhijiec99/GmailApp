/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.data;

/**
 * enumerator for the priority of the email.
 * @author Zhijie Cao
 */
public enum EmailPriority {
    PRIORITY_HIGHEST(1),
    PRIORITY_HIGH(2),
    PRIORITY_NORMAL(3),
    PRIORITY_LOW(4),
    PRIORITY_LOWEST(5);
    
    private int value;

    private EmailPriority(int i) {
        this.value = i;
    }
    
    /**
     * gets the value of the enum
     * @return 
     */
    public int getValue() {
        return this.value;
    }
    
    /**
     * Get an enum based on an index from 1-5
     * @param i, index from 1-5
     * @return the corresponding enum.
     * 
     */
    public static EmailPriority getPriority(int i){
        switch(i){
            case 1: return EmailPriority.PRIORITY_HIGHEST;
            case 2: return EmailPriority.PRIORITY_HIGH;
            case 3: return EmailPriority.PRIORITY_NORMAL;
            case 4: return EmailPriority.PRIORITY_LOW;
            case 5: return EmailPriority.PRIORITY_LOWEST;
            default: return EmailPriority.PRIORITY_NORMAL;
        }
    }
}
