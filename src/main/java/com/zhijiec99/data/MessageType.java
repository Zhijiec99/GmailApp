/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.data;

/**
 * This enumerator defines the type of an email: New, reply or forward.
 * @author Zhijie Cao
 */
public enum MessageType {
    NEW,
    REPLY,
    FORWARD;
    private int value;
    
    public int getValue(){
        return value;
    }
    
    public MessageType getMessageType(int value){
        switch(value){
            case 0: return MessageType.NEW;
            case 1: return MessageType.REPLY;
            case 2: return MessageType.REPLY;
            default: throw new IllegalArgumentException("Value not between 0-2, No messageType with value " + value);
        }
    }
}
