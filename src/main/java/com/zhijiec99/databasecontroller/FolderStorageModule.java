/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.databasecontroller;

import com.zhijiec99.JavaFx.fxbeans.*;
import com.zhijiec99.configurationcontroller.GMailConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class helps inserting, deleting and retrieving from the folder table.
 * @author Zhijie Cao
 */
public class FolderStorageModule extends DatabaseModule {
    
    public FolderStorageModule(FXPropertiesBean config) throws SQLException {
        super(config);
    }
    
    /**
     * Delete a folder.
     * @param name name of the folder to delete
     * @throws SQLException
     */
    public void deleteFolder(String name) throws SQLException{
        String query = "DELETE FROM FOLDER WHERE FOLDER_NAME = ?";
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.setString(1, name);
            ps.executeUpdate();
            LOG.info("Successfully deleted folder: \n" + name);
        }
    }
    
    /**
     * Retrieves the list of folders
     * @return List of folders in the database.
     * @throws SQLException 
     */
    public Map<Integer, String> retrieveFolder() throws SQLException{
        String query = "SELECT * FROM FOLDER";
        Map<Integer, String> result = new HashMap<>();
        try(PreparedStatement ps = connection.prepareStatement(query)){
            ps.executeQuery();
            ResultSet rs = ps.getResultSet();
            while(rs.next()){
                result.put(rs.getInt("folder_id"), rs.getString("folder_name"));
            }
        }
        return result;
    }
    
    /**
     * Creates a folder of a specific name;
     * @param folder
     * @throws SQLException 
     */
    public void createFolder(String folder) throws SQLException{
        String query = "INSERT INTO FOLDER(FOLDER_NAME) VALUES (?);";
        insertValuesHelper(query, new String[]{
            folder,
        });
    }
    
}
