/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.databasecontroller;

import com.zhijiec99.JavaFx.fxbeans.*;
import com.zhijiec99.beans.AttachmentBean;
import com.zhijiec99.beans.MailBean;
import com.zhijiec99.configurationcontroller.GMailConfig;
import com.zhijiec99.data.EmailPriority;
import com.zhijiec99.data.MessageType;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import jodd.mail.EmailAddress;

/**
 * This class is there to insert, and retrieve mailBeans.
 * @author Zhijie Cao
 */
public class MailStorageModule extends DatabaseModule {

    /**
     * Constructor for the mail storage
     *
     * @param config the GMailConfig object.
     */
    public MailStorageModule(FXPropertiesBean config) throws SQLException {
        super(config);
    }

    /**
     * Deletes a mail
     * @param bean bean containing the id of the mail to delete.
     * @throws SQLException 
     */
    public void deleteMail(MailBean bean) throws SQLException{
        String[] queries = new String[]{
            "DELETE FROM Attachments WHERE mailbean_id = ?",
            "DELETE FROM EMAIL_MAILBEAN where mailbean_id = ?",
            "DELETE FROM MAILBEAN WHERE mailBean_id = ?"
        };
        for(String query : queries){
            try (PreparedStatement pStatement = connection.prepareStatement(query);) {
                    pStatement.setInt(1, bean.getEmailId());
                    // A new try-with-resources block for creating the ResultSet object
                    // begins
                    pStatement.executeUpdate();
                }
        }
    }
    
    /**
     * Delete a mail from a list
     * @param beans the list of beans to delete
     * @throws SQLException 
     */
    public void deleteMail(List<MailBean> beans) throws SQLException{
        for(MailBean bean : beans){
            deleteMail(bean);
        }
    }
    /**
     * Overloaded retrieve mail for a single mailBean
     * @param bean the bean with the ID to check
     * @return the list of mailBean
     * @throws SQLException 
     */
    public List<MailBean> retrieveMail(MailBean bean) throws SQLException {
        return retrieveMail(new ArrayList<>(Arrays.asList(bean)));
    }

    public List<MailBean> retrieveMailByFolder(String folder) throws SQLException{
        List<MailBean> result = new ArrayList<>();
        String query = "SELECT * FROM MailBean m LEFT JOIN FOLDER f USING(folder_id) WHERE "
                + "f.folder_name = ?";
        try (PreparedStatement pStatement = connection.prepareStatement(query);) {
                pStatement.setString(1, folder);
                // A new try-with-resources block for creating the ResultSet object
                // begins
                ResultSet resultSet = pStatement.executeQuery();
                while(resultSet.next()) {
                    result.add(retrieveMail(new MailBean(resultSet.getInt("mailbean_id"))).get(0));
                }
            }
        return result;
    }
    /**
     * Retrieves the mail based on a list of mailBeans;
     *
     * @param mailBeans the list of mailBeans (with the valid IDs);
     * @return list of all the mailBean
     * @throws SQLException
     */
    public List<MailBean> retrieveMail(List<MailBean> mailBeans) throws SQLException {
        List<MailBean> result = new ArrayList<>();
        for (MailBean mailBean : mailBeans) {
            String query = "SELECT * FROM MailBean m"
                    + " LEFT JOIN email_address ea ON m.from_email_id = ea.email_id"
                    + " LEFT JOIN folder f ON f.folder_id = m.folder_id"
                    + " WHERE m.mailBean_id = ?";
            try (PreparedStatement pStatement = connection.prepareStatement(query);) {
                pStatement.setInt(1, mailBean.getEmailId());
                // A new try-with-resources block for creating the ResultSet object
                // begins
                ResultSet resultSet = pStatement.executeQuery();
                if (resultSet.next()) {
                    result.add(createBean(resultSet));
                }
            }
        }
        LOG.info("Retrieved " + result.size() + " rows.");
        return result;
    }

    /**
     * Overloaded method for a single mailBean
     *
     * @param bean the mailBean to insert
     * @throws SQLException
     */
    public int insertMail(MailBean bean) throws SQLException {
        List<Integer> ids = insertMail(new ArrayList<>(Arrays.asList(bean)));
        return (ids == null || ids.isEmpty() ? -1 : ids.get(0));
    }

    /**
     * Insert a list of mailBeans into the database.
     *
     * @param mailBeans mailBeans to send.
     * @throws SQLException
     */
    public List<Integer> insertMail(List<MailBean> mailBeans) throws SQLException {
        List<Integer> insertedIds = new ArrayList<>();
        for (MailBean bean : mailBeans) {

            int id;
            MailBean mail = verifyMailBean(bean);
            // makes sure all the EmailAddress are existant.
            checkAndInsertEmailAddress(mail.getBcc());
            checkAndInsertEmailAddress(mail.getCc());
            checkAndInsertEmailAddress(mail.getFrom());
            checkAndInsertEmailAddress(mail.getTo());

            //Make sure all the folder is existant.
            FolderStorageModule module = new FolderStorageModule(gmailConfig);
            Map<Integer, String> folders = module.retrieveFolder();
            int folderId;

            if (folders.containsValue(mail.getFolderName())) {
                folderId = folders.keySet().stream()
                        .filter(key -> mail.getFolderName().equalsIgnoreCase(folders.get(key)))
                        .findFirst()
                        .get();
            } else {
                folderId = folders.keySet().stream()
                        .filter(key -> "UNCATEGORIZED".equalsIgnoreCase(folders.get(key)))
                        .findFirst()
                        .get();
            }

            //Inserts the mailBean.
            String query = "INSERT INTO MAILBEAN"
                    + "(from_email_id, subject, text_message,"
                    + " html_Message, sent_Time, received_time,"
                    + " priority, folder_id, message_type) "
                    + "VALUES (?,?,?,?,?,?,?,?,?);";

            id = insertValuesHelper(query, new Object[]{
                "" + getEmailId(mail.getFrom()),
                mail.getSubject(),
                mail.getTextMessage(),
                mail.getHtmlMessage(),
                (mail.getSentTime() == null ? null : new Timestamp(mail.getSentTime().getTime())),
                (mail.getReceivedTime() == null ? null :new Timestamp(mail.getReceivedTime().getTime())),
                mail.getPriority().toString(),
                folderId,
                mail.getMessageType().toString()
            });
            String attachmentQuery = "INSERT INTO ATTACHMENTS(name, content, mailBean_id, embedded) VALUES(?, ?, ?, ?);";
            //Make sure all Attachments are existant.
            for (AttachmentBean attach : mail.getAttachments()) {
                insertValuesHelper(attachmentQuery, new Object[]{
                    attach.getName(),
                    attach.getContent(),
                    id,
                    false,});
            }

            for (AttachmentBean attach : mail.getEmbeddedAttachments()) {
                insertValuesHelper(attachmentQuery, new Object[]{
                    attach.getName(),
                    attach.getContent(),
                    id,
                    true,});
            }
            //make the links
            String emailMailBeanQuery = "INSERT INTO EMAIL_MAILBEAN(relation_type, EMAIL_ID, MAILBEAN_ID) VALUES(?, ?, ?)";
            for (EmailAddress to : mail.getTo()) {
                insertValuesHelper(emailMailBeanQuery, new Object[]{
                    "TO",
                    getEmailId(to),
                    id,});
            }
            for (EmailAddress cc : mail.getCc()) {
                insertValuesHelper(emailMailBeanQuery, new Object[]{
                    "CC",
                    getEmailId(cc),
                    id,});
            }
            for (EmailAddress bcc : mail.getBcc()) {
                insertValuesHelper(emailMailBeanQuery, new Object[]{
                    "BCC",
                    getEmailId(bcc),
                    id,});
            }
            insertedIds.add(id);
        }
        LOG.info("Inserted " + insertedIds.size() + " rows.");
        return insertedIds;
    }

    /**
     * gets an email ID based on its emailAddress object;
     *
     * @param emailAddress
     * @return id of email in database, or -1 if non-existant
     * @throws SQLException
     */
    private int getEmailId(EmailAddress emailAddress) throws SQLException {
        String query = "Select * from email_Address where email_Address = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, emailAddress.getEmail());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt("email_id");
            } else {
                return -1;
            }
        }
    }

    /**
     * This method makes sure the mailBean has all the fields to insert a
     * mailBean
     *
     * @param bean
     * @return
     */
    private MailBean verifyMailBean(MailBean bean) {
        if (bean.getFrom() == null) {
            throw new IllegalArgumentException("MailBean is missing a from");
        }
        MailBean verifiedBean = new MailBean(
                bean.getFrom(),
                (bean.getTo() == null ? new EmailAddress[0] : bean.getTo()),
                (bean.getCc() == null ? new EmailAddress[0] : bean.getCc()),
                (bean.getBcc() == null ? new EmailAddress[0] : bean.getBcc()),
                (bean.getSubject() == null || bean.getSubject() == "" ? "" : bean.getSubject()),
                (bean.getTextMessage() == null || bean.getTextMessage() == "" ? "" : bean.getTextMessage()),
                (bean.getHtmlMessage() == null || bean.getHtmlMessage() == "" ? "" : bean.getHtmlMessage()),
                (bean.getAttachments() == null ? new ArrayList<AttachmentBean>() : bean.getAttachments()),
                (bean.getEmbeddedAttachments() == null ? new ArrayList<AttachmentBean>() : bean.getEmbeddedAttachments()),
                (bean.getMessageType() == null ? MessageType.NEW : bean.getMessageType()),
                bean.getSentTime(),
                bean.getReceivedTime(),
                (bean.getFolderName() == null || bean.getFolderName().isEmpty() ? "UNCATEGORIZED" : bean.getFolderName()),
                (bean.getPriority() == null ? EmailPriority.PRIORITY_NORMAL : bean.getPriority())
        );
        return verifiedBean;
    }

    /**
     * Checks of an user exists and inserts them if they don't
     *
     * @param emailAddresses
     * @throws SQLException
     */
    private void checkAndInsertEmailAddress(EmailAddress[] emailAddresses) throws SQLException {
        String insertQuery = "INSERT INTO EMAIL_ADDRESS(EMAIL_ADDRESS, NAME) VALUES(?, ?)";
        for (EmailAddress email : emailAddresses) {
            if (getEmailId(email) == -1) {
                insertValuesHelper(insertQuery, new String[]{email.getEmail(), email.getPersonalName()});
            }
        }
    }

    /**
     * Overloaded method for checkAndInsertEmailAddress
     *
     * @param emailAddress
     */
    private void checkAndInsertEmailAddress(EmailAddress emailAddress) throws SQLException {
        checkAndInsertEmailAddress(new EmailAddress[]{emailAddress});
    }

    /**
     * Creates a bean from a result set
     *
     * @param rs result set to get the mailbean;
     * @return the mailbean of that result set.
     * @throws SQLException
     */
    private MailBean createBean(ResultSet rs) throws SQLException {
        int mailId = rs.getInt("mailbean_id");
        return new MailBean(
                new EmailAddress(rs.getString("ea.name"), rs.getString("ea.email_address")),
                getAddressByMailId(mailId, "TO"),
                getAddressByMailId(mailId, "CC"),
                getAddressByMailId(mailId, "BCC"),
                rs.getString("subject"),
                rs.getString("text_message"),
                rs.getString("html_message"),
                getAttachments(mailId, false),
                getAttachments(mailId, true),
                MessageType.valueOf(rs.getString("message_type")),
                rs.getTimestamp("sent_time"),
                rs.getTimestamp("received_time"),
                rs.getString("folder_name"),
                EmailPriority.valueOf(rs.getString("priority")),
                rs.getInt("mailBean_id")
        );
    }

    /**
     *
     * @param mailId the id of the email the address is associated to
     * @param type the way the address is associated to the email (TO, CC, BCC)
     * @return EmailAddress[] the list of email
     */
    private EmailAddress[] getAddressByMailId(int mailId, String type) throws SQLException {
        List<EmailAddress> emailAddresses = new ArrayList<>();
        String query = "SELECT * FROM MailBean m"
                + " RIGHT JOIN EMAIL_MAILBEAN em USING(mailbean_id) "
                + " LEFT JOIN EMAIL_ADDRESS USING(email_id)"
                + " WHERE em.mailBean_id = ? AND em.relation_type = ? ";

        try (PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, mailId);
            pStatement.setString(2, type.toUpperCase());
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    emailAddresses.add(new EmailAddress(resultSet.getString("name"), resultSet.getString("email_address")));
                }
            }
        }
        return emailAddresses.toArray(new EmailAddress[emailAddresses.size()]);
    }

    /**
     * Gets the Attachment from a mailId.
     *
     * @param mailId, the mail the attachment is attached on
     * @param embedded if the attachment is embedded or not.
     * @return List of all the attachmentBeans
     * @throws SQLException
     */
    private List<AttachmentBean> getAttachments(int mailId, boolean embedded) throws SQLException {
        List<AttachmentBean> results = new ArrayList<>();
        String query = "SELECT * FROM attachments where mailBean_id = ?";

        try (PreparedStatement pStatement = connection.prepareStatement(query)) {
            pStatement.setInt(1, mailId);
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    if (resultSet.getBoolean("embedded") == embedded) {
                        Blob content = resultSet.getBlob("content");
                        byte[] data = content.getBytes(1, (int) content.length());
                        content.free();
                        results.add(new AttachmentBean(resultSet.getString("name"), data));
                    }
                }
            }
        }
        return results;
    }
}
