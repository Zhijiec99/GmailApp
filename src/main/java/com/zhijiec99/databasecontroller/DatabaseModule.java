/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zhijiec99.databasecontroller;

import com.zhijiec99.JavaFx.fxbeans.FXPropertiesBean;
import com.zhijiec99.configurationcontroller.GMailConfig;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import javax.sql.rowset.serial.SerialBlob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is the parent class for a module dealing with a database.
 * @author Zhijie Cao
 */
public class DatabaseModule {

    protected FXPropertiesBean gmailConfig;
    protected Connection connection;
    protected Logger LOG = LoggerFactory.getLogger(this.getClass().getName());

    /**
     * Constructor for a databaseModule
     *
     * @param config the configuration object
     */
    public DatabaseModule(FXPropertiesBean config) throws SQLException {
        if (config == null) {
            throw new IllegalArgumentException("config cannot be null!");
        }
        this.gmailConfig = config;
        this.connection = DriverManager.getConnection(gmailConfig.generateDatabaseURL(), gmailConfig.getDatabaseUserName(), gmailConfig.getDatabasePassword());
        connection.prepareStatement("USE " + config.getDatabaseName()).execute();
    }

    /**
     * Helper method to insert values base on a query and its values
     *
     * @param query Query of insert.
     * @param values
     * @return The key of the object it just inserted
     * @throws SQLException
     */
    protected int insertValuesHelper(String query, Object[] values) throws SQLException {
        try (PreparedStatement stmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof String) {
                    stmt.setString(i + 1, values[i].toString());
                } else if (values[i] instanceof byte[]) {
                    Blob b = new SerialBlob((byte[]) values[i]);
                    stmt.setBlob(i + 1, b);
                } else if (values[i] instanceof Boolean) {
                    stmt.setBoolean(i + 1, (Boolean) values[i]);
                } else if (values[i] instanceof Integer) {
                    stmt.setInt(i + 1, (int) values[i]);
                } else if (values[i] instanceof Timestamp) {
                    stmt.setTimestamp(i + 1, (Timestamp) values[i]);
                } else if (values[i] == null) {
                    stmt.setObject(i + 1, null);
                }
            }
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            LOG.info("Successfully inserted value: \n" + query);
            return rs.next() ? rs.getInt(1) : -1;
        }
    }
}
