-- Best practice MySQL as of 5.7.6
--
-- The Aquarium database must exist before running this script
-- and before running the fx_desktop_standard_project because the unit
-- test runs this script. Only the root user can create a MySQL database
-- but you do not want to use the root user and password in your code.
--
-- This script needs to run only once

DROP DATABASE IF EXISTS RECEIVE;
CREATE DATABASE RECEIVE;

USE RECEIVE;

DROP USER IF EXISTS RECEIVE@localhost;
CREATE USER RECEIVE@'localhost' IDENTIFIED WITH mysql_native_password BY 'zhijie123' REQUIRE NONE;
GRANT ALL ON RECEIVE.* TO RECEIVE@'localhost';

-- This creates a user with access from any IP number except localhost
-- Use only if your MyQL database is on a different host from localhost
-- DROP USER IF EXISTS fish;
-- CREATE USER fish IDENTIFIED WITH mysql_native_password BY 'kfstandard' REQUIRE NONE;
-- GRANT ALL ON AQUARIUM TO fish;

-- This creates a user with access from a specific IP number
-- Preferable to '%'
-- DROP USER IF EXISTS fish@'192.168.0.194';
-- CREATE USER fish@'192.168.0.194' IDENTIFIED WITH mysql_native_password BY 'kfstandard' REQUIRE NONE;
-- GRANT ALL ON AQUARIUM TO fish@'192.168.0.194';

FLUSH PRIVILEGES;

-- @Author Zhi Jie Cao
-- Last modification date: 2018-09-26 23:37:27.761
-- tables
-- Table: Attachments

DROP TABLE IF EXISTS Email_MailBean;
DROP TABLE IF EXISTS Attachments;
DROP TABLE IF EXISTS MailBean;
DROP TABLE IF EXISTS Folder;
DROP TABLE IF EXISTS Email_Address;


CREATE TABLE Attachments (
    attachment_id int NOT NULL AUTO_INCREMENT,
    name text,
    content LONGBLOB NOT NULL,
    mailbean_id int NOT NULL,
    embedded bool NOT NULL,
    CONSTRAINT Attachments_pk PRIMARY KEY (attachment_id)
);

-- Table: Email_Address
CREATE TABLE Email_Address (
    email_id int NOT NULL AUTO_INCREMENT,
    Email_address text NOT NULL,
    Name text,
    CONSTRAINT Email_Address_pk PRIMARY KEY (email_id)
);

-- Table: Email_MailBean
CREATE TABLE Email_MailBean (
    relation_type text NOT NULL,
    email_id int NOT NULL,
    mailbean_id int NOT NULL,
    CONSTRAINT Email_MailBean_pk PRIMARY KEY (email_id,mailbean_id,relation_type(20))
);

-- Table: Folder
CREATE TABLE Folder (
    folder_id int NOT NULL AUTO_INCREMENT,
    folder_name text NOT NULL,
    CONSTRAINT Folder_pk PRIMARY KEY (folder_id)
);

-- Table: MailBean
CREATE TABLE MailBean (
    mailbean_id int NOT NULL AUTO_INCREMENT,
    from_email_id int NULL,
    subject text NULL,
    text_message text NOT NULL,
    html_message text NOT NULL,
    sent_time timestamp NULL,
    received_time timestamp NULL,
    priority text NOT NULL,
    folder_id int NULL,
    message_type text NULL,
    CONSTRAINT MailBean_pk PRIMARY KEY (mailbean_id)
);

-- foreign keys
-- Reference: Attachments_MailBean (table: Attachments)
ALTER TABLE Attachments ADD CONSTRAINT Attachments_MailBean FOREIGN KEY Attachments_MailBean (mailbean_id)
    REFERENCES MailBean (mailbean_id);

-- Reference: Email_Address_MailBean (table: MailBean)
ALTER TABLE MailBean ADD CONSTRAINT Email_Address_MailBean FOREIGN KEY Email_Address_MailBean (from_email_id)
    REFERENCES Email_Address (email_id);

-- Reference: Email_MailBean_Email_Address (table: Email_MailBean)
ALTER TABLE Email_MailBean ADD CONSTRAINT Email_MailBean_Email_Address FOREIGN KEY Email_MailBean_Email_Address (email_id)
    REFERENCES Email_Address (email_id);

-- Reference: Email_MailBean_MailBean (table: Email_MailBean)
ALTER TABLE Email_MailBean ADD CONSTRAINT Email_MailBean_MailBean FOREIGN KEY Email_MailBean_MailBean (mailbean_id)
    REFERENCES MailBean (mailbean_id);

-- Reference: Folder_MailBean (table: MailBean)
ALTER TABLE MailBean ADD CONSTRAINT Folder_MailBean FOREIGN KEY Folder_MailBean (folder_id)
    REFERENCES Folder (folder_id);

-- End of file.
INSERT INTO FOLDER(folder_name) VALUES('INBOX');
INSERT INTO FOLDER(folder_name) VALUES('SENT');
INSERT INTO FOLDER(folder_name) VALUES('UNCATEGORIZED');


